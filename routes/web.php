<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('site.home');
});

Route::get('/sobre', function () {
    return view('site.sobre');
});

Route::get('/plano-classico', function () {
    return view('site.plano-classico');
});

Route::get('/plano-empresarial', function () {
    return view('site.plano-empresarial');
});

Route::get('/plano-classico', function () {
    return view('site.plano-classico');
});

Route::get('/servicos', function () {
    return view('site.servicos');
});

Route::get('/servicos-integra', function () {
    return view('site.servicos-integra');
});

Route::get('/faq', function () {
    return view('site.faq');
});

Route::get('/parceiros', function () {
    return view('site.parceiros');
});

Route::get('/parceiros-integra', function () {
    return view('site.parceiros-integra');
});

Route::get('/depoimentos', function () {
    return view('site.depoimentos');
});

Route::get('/noticias', function () {
    return view('site.noticias');
});

Route::get('/noticias-integra', function () {
    return view('site.noticias-integra');
});

Route::get('/unidades', function () {
    return view('site.unidades');
});

Route::get('/contato', function () {
    return view('site.contato');
});

Route::get('/cliente', function () {
    return view('site.area-cliente');
});

