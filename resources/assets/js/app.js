$(document).ready(function(){
  $('.depoimentos-carousel').slick({
    mobileFirst: true,
    autoplay: true,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: '<button class="depoimentos-carousel-arrow slick-prev slick-arrow" aria-label="Anterior" type="button"><i class="fas fa-chevron-left"></i></button>',
    nextArrow: '<button class="depoimentos-carousel-arrow slick-next slick-arrow" aria-label="Próximo" type="button"><i class="fas fa-chevron-right"></i></button>',
    responsive: [
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        }
    ]
  });
});

// Formulário
(function() {
    'use strict';
    window.addEventListener('load', function() {
      // Pega todos os formulários que nós queremos aplicar estilos de validação Bootstrap personalizados.
      var forms = document.getElementsByClassName('needs-validation');
      // Faz um loop neles e evita o envio
      var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
          if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
          }
          form.classList.add('was-validated');
        }, false);
      });
    }, false);
  })();
