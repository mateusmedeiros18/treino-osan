<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <title>@yield('title') | OSAN</title>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="{{asset('css/libs/bootstrap.min.css')}}">

        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{asset('css/libs/all.min.css')}}">

        <!-- Slick -->
        <link rel="stylesheet" href="{{asset('css/libs/slick.css')}}">
        <link rel="stylesheet" href="{{asset('css/libs/slick-theme.css')}}">

        <!-- CSS -->
        <link rel="stylesheet" href="{{asset('css/style.css')}}">
    </head>
    <body>

        <!-- HEADER -->
        @include('site.includes.header')

        <!-- CONTENT -->
        <main>
            @yield('content')
        </main>

        <!-- FOOTER -->
        @include('site.includes.footer')

        <!-- Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="{{asset('js/libs/bootstrap.min.js')}}"></script>

        <!-- Font Awesome -->
        <script data-search-pseudo-elements src="{{asset('js/libs/all.min.js')}}"></script>

        <!-- Slick -->
        <script src="{{asset('js/libs/slick.min.js')}}"></script>

        <!-- JS -->
        <script src="{{asset('js/main.js')}}"></script>
    </body>
</html>
