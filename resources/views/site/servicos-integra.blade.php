@extends('layouts.app')

@section('title', 'Serviços Íntegra')

@section('content')
    <div class="container page" id="page-servicos">
        <div class="page-nav page-nav-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-12 pt-2 pt-lg-3">
                        <p class="page-nav-text">Você está em: <a href="{{url('/')}}" class="page-nav-link">Home</a> <a href="{{url('/servicos')}}" class="page-nav-link page-nav-link-active">Serviços</a></p>
                    </div>
                </div>
                <div class="row pt-2 pt-lg-4 pb-2 pb-lg-5">
                    <div class="col-12">
                        <h1 class="page-wrapper-title">Serviços</h1>
                    </div>
                </div>
            </div>
        </div>

        <div class="container pl-md-0 page-wrapper">
            <div class="row pb-3 pb-md-5">
                <div class="col-md-6">
                    <img src="{{asset('images/translado-aereo-destaque.png')}}" alt="Translado Aéreo" class="servicos-integra-destaque">
                </div>
                <div class="col-md-6 px-4 pl-md-0 pt-5">
                    <div class="servicos-integra-title-container">
                        <i class="fas fa-plane-departure servicos-integra-title-icon"></i>
                        <h2 class="servicos-integra-title">Translado aéreo</h2>
                    </div>
                    <p class="servicos-integra-text">Com parceria com várias companhias aéreas no Brasil, conseguimos executar o transporte aéreo com todos os serviços funerários inclusos. Somos uma das poucas empresas a ter o melhor preço de transporte funerário aéreo no Brasil.</p>
                    <p class="servicos-integra-text">Com essa parceria, além da comodidade de ter todos os serviços funerários inclusos, conseguimos um preço padrão para todos os serviços. Somos empresa líder no transporte de corpo em avião comercial.</p>
                    <a href="{{url('/contato')}}" title="Solicitar orçamento com a Osan" class="btn btn-blue-dark servicos-integra-btn">Solicite um orçamento agora mesmo!</a>
                </div>
            </div>

            <div class="container my-5 px-3 px-md-5">
                <div class="row">
                    <div class="col-12">
                        <h3 class="servicos-integra-title servicos-integra-title-special mb-2 mb-md-4">Serviços oferecidos</h3>
                        <p class="servicos-integra-text">Nos serviços de translados funerários aéreos em avião em vôos comerciais é usado os seguintes seriços funerários</p>
                    </div>
                </div>
                <div class="row pt-3 pt-md-5 servicos-integra-item-container">
                    <div class="col-md-4 servicos-integra-item">
                        <h4 class="servicos-integra-item-title"><i class="far fa-sun servicos-integra-item-icon"></i> Urna</h4>
                        <p class="servicos-integra-item-desc">Caixão zincado</p>
                    </div>
                    <div class="col-md-4 servicos-integra-item">
                        <h4 class="servicos-integra-item-title"><i class="far fa-sun servicos-integra-item-icon"></i> Preparo</h4>
                        <p class="servicos-integra-item-desc">Embalsamento do óbito</p>
                    </div>
                    <div class="col-md-4 servicos-integra-item">
                        <h4 class="servicos-integra-item-title"><i class="far fa-sun servicos-integra-item-icon"></i> Ata</h4>
                        <p class="servicos-integra-item-desc">Ata de embalsamento</p>
                    </div>
                </div>
                <div class="row pb-3 pb-md-5 servicos-integra-item-container">
                    <div class="col-md-4 servicos-integra-item">
                        <h4 class="servicos-integra-item-title"><i class="far fa-sun servicos-integra-item-icon"></i> Agilidade</h4>
                        <p class="servicos-integra-item-desc">Acompanhamento de todo processo de embarque</p>
                    </div>
                    <div class="col-md-4 servicos-integra-item">
                        <h4 class="servicos-integra-item-title"><i class="far fa-sun servicos-integra-item-icon"></i> Desembarque</h4>
                        <p class="servicos-integra-item-desc">Desembarque na cidade de destino</p>
                    </div>
                    <div class="col-md-4 servicos-integra-item">
                        <h4 class="servicos-integra-item-title"><i class="far fa-sun servicos-integra-item-icon"></i> Transporte</h4>
                        <p class="servicos-integra-item-desc">Translado até o aeroporto de embarque</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
