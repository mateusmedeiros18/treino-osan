@extends('layouts.app')

@section('title', 'Área do Cliente')

@section('content')
    <div class="container page" id="page-client">
        <div class="page-nav page-nav-wrapper mb-2 mb-lg-3">
            <div class="container">
                <div class="row">
                    <div class="col-12 pt-2 pt-lg-3">
                        <p class="page-nav-text">Você está em: <a href="{{url('/')}}" class="page-nav-link">Home</a> <a href="{{url('/cliente')}}" class="page-nav-link page-nav-link-active">Área do Cliente</a></p>
                    </div>
                </div>
            </div>
        </div>

        <div class="container page-wrapper py-5 pb-lg-0">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7">
                        <h1 class="page-wrapper-title">Contratos</h1>
                        <p class="page-wrapper-text">Confiram os contratos dos serviços oferecidos pela OSAN.</p>

                        <div class="mt-5 contratos">
                            <div class="row contratos-row">
                                <a href="#" title="Conferir contrato" class="col-md-6 col-lg-3 contratos-item">
                                    <img src="{{asset('images/icones/contrato.png')}}">
                                    <p>Contrato de número <span>00001</span> atualizado em 2018</p>
                                </a>
                                <a href="#" title="Conferir contrato" class="col-md-6 col-lg-3 contratos-item">
                                    <img src="{{asset('images/icones/contrato.png')}}">
                                    <p>Contrato de número <span>00001</span></p>
                                </a>
                                <a href="#" title="Conferir contrato" class="col-md-6 col-lg-3 contratos-item">
                                    <img src="{{asset('images/icones/contrato.png')}}">
                                    <p>Contrato de número <span>00001</span> atualizado em 2018</p>
                                </a>
                                <a href="#" title="Conferir contrato" class="col-md-6 col-lg-3 contratos-item">
                                    <img src="{{asset('images/icones/contrato.png')}}">
                                    <p>Contrato de número <span>00001</span></p>
                                </a>
                            </div>
                            <div class="row contratos-row">
                                <a href="#" title="Conferir contrato" class="col-md-6 col-lg-3 contratos-item">
                                    <img src="{{asset('images/icones/contrato.png')}}">
                                    <p>Contrato de número <span>00001</span></p>
                                </a>
                                <a href="#" title="Conferir contrato" class="col-md-6 col-lg-3 contratos-item">
                                    <img src="{{asset('images/icones/contrato.png')}}">
                                    <p>Contrato de número <span>00001</span> atualizado em 2018</p>
                                </a>
                                <a href="#" title="Conferir contrato" class="col-md-6 col-lg-3 contratos-item">
                                    <img src="{{asset('images/icones/contrato.png')}}">
                                    <p>Contrato de número <span>00001</span></p>
                                </a>
                                <a href="#" title="Conferir contrato" class="col-md-6 col-lg-3 contratos-item">
                                    <img src="{{asset('images/icones/contrato.png')}}">
                                    <p>Contrato de número <span>00001</span> atualizado em 2018</p>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 offset-lg-1">
                        <h2 class="page-wrapper-title">Segunda via do boleto</h2>
                        <p class="page-wrapper-text">Caso seja necessário, gere a 2ª via de seus boletos.</p>
                        <a href="#" title="Acessar a segunda via do seu boleto" class="btn btn-normal btn-blue-dark">Acessar o Boleto</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
