<footer class="container-fluid mt-4 footer">
    <div class="row">
        <div class="container">
            <div class="row">
                <!-- Logo -->
                <div class="col-lg-2 d-none d-lg-flex align-items-center">
                    <a href="{{url('/')}}" title="Acessar página inicial">
                        <img src="{{asset('images/logo.png')}}" alt="Logo da OSAN" class="logo-img">
                    </a>
                </div>
                <!-- Acesso Rápido -->
                <div class="col-lg-4">
                    <h2 class="footer-title">Acesso rápido</h2>
                    <div class="row acesso-lista-row">
                        <div class="col-6 acesso-lista-container">
                            <ul class="acesso-lista">
                                <li class="acesso-item"><a href="{{url('/sobre')}}" title="Conheça a Osan" class="acesso-link">A Osan</a></li>
                                <li class="acesso-item"><a href="{{url('/plano-classico')}}" title="Confira os planos da Osan" class="acesso-link">Planos</a></li>
                                <li class="acesso-item"><a href="{{url('/servicos')}}" title="Confira os serviços da Osan" class="acesso-link">Serviços</a></li>
                                <li class="acesso-item"><a href="{{url('/faq')}}" title="Consultar dúvidas frequentes" class="acesso-link">Dúvidas</a></li>
                                <li class="acesso-item"><a href="{{url('/parceiros')}}" title="Conheça os parceiros da Osan" class="acesso-link">Parceiros</a></li>
                            </ul>
                        </div>
                        <div class="col-6 acesso-lista-container">
                            <ul class="acesso-lista">
                                <li class="acesso-item"><a href="{{url('/depoimentos')}}" title="Confira os depoimentos dos clientes da Osan" class="acesso-link">Depoimentos</a></li>
                                <li class="acesso-item"><a href="{{url('/noticias')}}" title="Confira as notícias da Osan" class="acesso-link">Notícias</a></li>
                                <li class="acesso-item"><a href="{{url('/unidades')}}" title="Confira as unidades da Osan" class="acesso-link">Unidades</a></li>
                                <li class="acesso-item"><a href="{{url('/contato')}}" title="Entre em contato com a Osan" class="acesso-link">Contatos</a></li>
                                <li class="acesso-item"><a href="https://www.catho.com.br/empregos/osan" target="_blank" title="Trabalhe na Osan, confira nossas vagas disponíveis" class="acesso-link">Trabalhe Conosco</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- Botões -->
                <div class="col-lg-4 d-none d-lg-flex footer-btn">
                    <!-- Falecimento -->
                    <a href="tel:08000178000" title="Ligar para a Osan" class="col-12 mr-3 d-flex footer-btn-item footer-btn-item-primary">
                        <div class="pl-lg-2 pl-xl-5 footer-btn-icon-container">
                            <i class="fas fa-phone-alt footer-btn-icon footer-btn-icon-phone footer-btn-icon-phone-primary"></i>
                        </div>
                        <div class="pr-lg-2 pl-lg-3 footer-btn-text">
                            <p><span class="footer-btn-text-small">Em caso de falecimento ligue</span><br>
                                0800-017 8000</p>
                        </div>
                    </a>

                    <!-- Whatsapp -->
                    <a href="https://api.whatsapp.com/send?phone=551399768870" title="Falar por whatsapp com a Osan" target="_blank" class="col-12 mr-3 d-flex footer-btn-item">
                        <div class="pl-lg-2 pl-xl-5 footer-btn-icon-container">
                            <i class="fab fa-whatsapp footer-btn-icon"></i>
                        </div>
                        <div class="pr-lg-0 pl-lg-3 footer-btn-text">
                            <p><span class="footer-btn-text-small">Fale conosco via whatsapp</span><br>
                                (13) 99768870</p>
                        </div>
                    </a>

                    <!-- Atendimento -->
                    <a href="tel:1332288000" title="Ligar para a Osan" class="col-12 mr-3 d-flex footer-btn-item">
                        <div class="pl-lg-2 pl-xl-5 footer-btn-icon-container">
                            <i class="fas fa-phone-alt footer-btn-icon footer-btn-icon-phone"></i>
                        </div>
                        <div class="pr-lg-2 pl-lg-3 footer-btn-text">
                            <p><span class="footer-btn-text-small">Atendimento</span><br>
                                (13) 3228-8000</p>
                        </div>
                    </a>
                </div>
                <!-- Redes Sociais -->
                <div class="col-lg-2 footer-social">
                    <div class="row">
                        <div class="col-12 col-md-6 col-lg-12">
                            <h2 class="footer-title text-center">Acompanhe nas redes sociais</h2>
                        </div>
                        <div class="col-12 col-md-6 col-lg-12 d-flex align-items-center justify-content-around footer-social-container">
                            <a href="https://www.facebook.com/osanoficial" title="Curta nossa página no Facebook" target="_blank" class="footer-social-link">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                            <a href="https://twitter.com/" title="Siga-nos no Twitter" target="_blank" class="footer-social-link">
                                <i class="fab fa-twitter"></i>
                            </a>
                            <a href="https://br.linkedin.com/" title="Visualize nosso perfil no Linkedin" target="_blank" class="d-lg-none footer-social-link">
                                <i class="fab fa-linkedin-in"></i>
                            </a>
                        </div>
                    </div>
                    <div class="row footer-copy-container">
                        <div class="col-12">
                            <p class="footer-copy">Desenvolvido por <a href="https://kbrtec.com.br" title="Visitar site da KBRTEC" target="_blank" class="footer-copy-link">KBRTEC</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
