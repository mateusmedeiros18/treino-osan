<!-- Info Mobile -->
<div class="container-fluid">
    <div class="row d-lg-none">
        <a href="tel:08000178000" title="Ligar para a Osan" class="col-6 col-sm-5 d-flex info-container info-tel">
            <div class="pl-md-3 info-icon-container">
                <i class="fas fa-phone-alt info-icon info-icon-phone"></i>
            </div>
            <div class="px-2 pl-md-4 info-text">
                <p><span class="info-text-small">Em caso de falecimento ligue</span><br>
                    0800-017 8000</p>
            </div>
        </a>
        <a href="https://api.whatsapp.com/send?phone=551399768870" title="Falar por whatsapp com a Osan" target="_blank" class="col-6 col-sm-4 d-flex info-container info-whats">
            <div class="pl-md-3 info-icon-container">
                <i class="fab fa-whatsapp info-icon"></i>
            </div>
            <div class="px-2 pl-md-4 info-text">
                <p><span class="info-text-small">Clique aqui para falar</span><br>
                    por whatsapp</p>
            </div>
        </a>
        <div class="col-12 col-sm-3 d-flex justify-content-around info-social">
            <a href="https://www.facebook.com/osanoficial" title="Curta nossa página no Facebook" target="_blank" class="info-social-link">
                <i class="fab fa-facebook-f"></i>
            </a>
            <a href="https://twitter.com/" title="Siga-nos no Twitter" target="_blank" class="info-social-link">
                <i class="fab fa-twitter"></i>
            </a>
            <a href="https://br.linkedin.com/" title="Visualize nosso perfil no Linkedin" target="_blank" class="info-social-link">
                <i class="fab fa-linkedin-in"></i>
            </a>
        </div>
    </div>
</div>

<!-- Navbar Mobile -->
<div class="container">
    <div class="row d-lg-none align-items-center pt-4 pb-2">
        <button class="col-4 navbar-toggler" type="button" data-toggle="collapse" data-target="#menuMobile" aria-controls="menuMobile" aria-expanded="false" aria-label="Alterna navegação">
            <span class="menu-mobile-btn"><img src="{{asset('images/icones/menu.png')}}" class="menu-mobile-icon pr-2">Menu</span>
        </button>

        <div class="col-4 d-flex justify-content-center logo-mobile">
            <a href="{{url('/')}}" title="Acessar página inicial">
                <img src="{{asset('images/logo.png')}}" alt="Logo da OSAN" class="logo-mobile-img">
            </a>
        </div>

        <div class="col-4 d-flex justify-content-end">
            <a href="{{url('/cliente')}}" class="menu-mobile-client" title="Acessar área do cliente">Área do Cliente<img src="{{asset('images/icones/user.png')}}" class="menu-mobile-icon pl-2"></a>
        </div>

        <!-- Menu mobile -->
        <nav class="navbar menu-mobile-container d-lg-none">
            <div class="collapse navbar-collapse" id="menuMobile">
                <ul class="navbar-nav menu-mobile">
                    <li class="nav-item"><a href="{{url('/sobre')}}" title="Conheça a Osan" class="nav-link menu-mobile-link">A Osan</a></li>
                    <!--<li class="nav-item"><a href="" title="Confira os planos da Osan" class="nav-link menu-mobile-link">Planos</a></li>-->
                    <li class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle menu-mobile-link dropdown-toggle-mobile" id="menuDrop" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Planos</a>
                        <div class="dropdown-menu dropdown-menu-mobile" aria-labelledby="menuDrop">
                            <a class="dropdown-item dropdown-link dropdown-link-mobile" href="{{url('/plano-classico')}}" title="Confira o plano clássico da Osan">Plano Clássico</a>
                            <a class="dropdown-item dropdown-link dropdown-link-mobile" href="{{url('/plano-empresarial')}}" title="Confira o plano empresarial da Osan">Plano Empresarial</a>
                        </div>
                    </li>
                    <li class="nav-item"><a href="{{url('/servicos')}}" title="Confira os serviços da Osan" class="nav-link menu-mobile-link">Serviços</a></li>
                    <li class="nav-item"><a href="{{url('/faq')}}" title="Consultar dúvidas frequentes" class="nav-link menu-mobile-link">Dúvidas</a></li>
                    <li class="nav-item"><a href="{{url('/parceiros')}}" title="Conheça os parceiros da Osan" class="nav-link menu-mobile-link">Parceiros</a></li>
                    <li class="nav-item"><a href="{{url('/depoimentos')}}" title="Confira os depoimentos dos clientes da Osan" class="nav-link menu-mobile-link">Depoimentos</a></li>
                    <li class="nav-item"><a href="{{url('/noticias')}}" title="Confira as notícias da Osan" class="nav-link menu-mobile-link">Notícias</a></li>
                    <li class="nav-item"><a href="{{url('/unidades')}}" title="Confira as unidades da Osan" class="nav-link menu-mobile-link">Unidades</a></li>
                    <li class="nav-item"><a href="{{url('/contato')}}" title="Entre em contato com a Osan" class="nav-link menu-mobile-link">Contatos</a></li>
                </ul>
            </div>
        </nav>
    </div>
</div>

<!-- Info desktop -->
<div class="container-fluid d-none d-lg-block">
    <div class="row info-header">
        <div class="container">
            <div class="row align-items-center justify-content-around py-3">
                <div class="col-lg-2 d-flex justify-content-start logo">
                    <a href="{{url('/')}}" title="Acessar página inicial">
                        <img src="{{asset('images/logo.png')}}" alt="Logo da OSAN" class="logo-img">
                    </a>
                </div>

                <a href="tel:08000178000" title="Ligar para a Osan" class="col-lg mr-3 d-flex info-container info-tel">
                    <div class="pl-lg-2 pl-xl-4 info-icon-container">
                        <i class="fas fa-phone-alt info-icon info-icon-phone"></i>
                    </div>
                    <div class="pr-lg-2 pl-lg-3 pl-xl-4 info-text">
                        <p><span class="info-text-small">Em caso de falecimento ligue</span><br>
                            0800-017 8000</p>
                    </div>
                </a>

                <a href="https://api.whatsapp.com/send?phone=551399768870" title="Falar por whatsapp com a Osan" target="_blank" class="col-lg d-flex info-container info-whats">
                    <div class="pl-lg-4 pl-xl-5 info-icon-container">
                        <i class="fab fa-whatsapp info-icon"></i>
                    </div>
                    <div class="pr-lg-0 pl-lg-4 info-text">
                        <p><span class="info-text-small">Clique aqui para falar</span><br>
                            por whatsapp</p>
                    </div>
                </a>

                <div class="col-lg-3 d-flex justify-content-end">
                    <a href="{{url('/cliente')}}" title="Acessar área do cliente" class="menu-client">Área do Cliente<img src="{{url('images/icones/user.png')}}" class="menu-icon pl-3"></a>
                </div>
            </div>
        </div>
    </div>

    <!-- Navbar desktop -->
    <div class="row menu-row">
        <div class="container">
            <div class="row">
                <nav class="col-lg-10 navbar navbar-expand-lg menu-container">
                    <ul class="navbar-nav menu">
                        <li class="nav-item"><a href="{{url('/sobre')}}" title="Conheça a Osan" class="nav-link menu-link">A Osan</a></li>
                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link menu-link dropdown-toggle" id="menuDrop" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Planos</a>
                            <div class="dropdown-menu" aria-labelledby="menuDrop">
                                <a class="dropdown-item dropdown-link" href="{{url('/plano-classico')}}" title="Confira o plano clássico da Osan">Plano Clássico</a>
                                <a class="dropdown-item dropdown-link" href="{{url('/plano-empresarial')}}" title="Confira o plano empresarial da Osan">Plano Empresarial</a>
                            </div>
                        </li>
                        <li class="nav-item"><a href="{{url('/servicos')}}" title="Confira os serviços da Osan" class="nav-link menu-link">Serviços</a></li>
                        <li class="nav-item"><a href="{{url('/faq')}}" title="Consultar dúvidas frequentes" class="nav-link menu-link">Dúvidas</a></li>
                        <li class="nav-item"><a href="{{url('/parceiros')}}" title="Conheça os parceiros da Osan" class="nav-link menu-link">Parceiros</a></li>
                        <li class="nav-item"><a href="{{url('/depoimentos')}}" title="Confira os depoimentos dos clientes da Osan" class="nav-link menu-link">Depoimentos</a></li>
                        <li class="nav-item"><a href="{{url('/noticias')}}" title="Confira as notícias da Osan" class="nav-link menu-link">Notícias</a></li>
                        <li class="nav-item"><a href="{{url('/unidades')}}" title="Confira as unidades da Osan" class="nav-link menu-link">Unidades</a></li>
                        <li class="nav-item"><a href="{{url('/contato')}}" title="Entre em contato com a Osan" class="nav-link menu-link">Contatos</a></li>
                    </ul>
                </nav>

                <div class="col-lg-1 offset-lg-1 d-flex justify-content-end info-social">
                    <a href="https://www.facebook.com/osanoficial" title="Curta nossa página no Facebook" target="_blank" class="info-social-link">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                    <a href="https://twitter.com/" title="Siga-nos no Twitter" target="_blank" class="info-social-link">
                        <i class="fab fa-twitter"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
