@extends('layouts.app')

@section('title', 'Notícias Íntegra')

@section('content')
    <div class="container page">
        <div class="page-nav page-nav-wrapper mb-2 mb-lg-3">
            <div class="container">
                <div class="row">
                    <div class="col-12 pt-2 pt-lg-3">
                        <p class="page-nav-text">Você está em: <a href="{{url('/')}}" class="page-nav-link">Home</a> <a href="{{url('/noticias')}}" class="page-nav-link page-nav-link-active">Notícias</a></p>
                    </div>
                </div>
                <div class="row pt-2 pt-lg-4 pb-2 pb-lg-3">
                    <div class="col-12">
                        <time class="noticia-date" datetime=”2017-05-17”>17 de maio de 2017</time>
                        <h1 class="mt-1 page-wrapper-title">Lidando com o luto de um colega de trabalho</h1>
                    </div>
                </div>
            </div>
        </div>

        <div class="container px-0 page-wrapper mb-5">
            <div class="row">
                <div class="col-12">
                    <img src="{{asset('images/noticias/destaque.png')}}" alt="" class="img-fluid w-100 noticia-integra-destaque">
                </div>
            </div>

            <div class="container px-3 px-lg-5">
                <div class="row">
                    <div class="col-12 noticia-integra-share">
                        <a href="#" title="Compartilhe no Facebook" target="_blank" class="noticia-integra-share-icon">
                        <i class="fab fa-facebook-f"></i>
                        </a>
                        <a href="#" title="Compartilhe no Twitter" target="_blank" class="noticia-integra-share-icon">
                            <i class="fab fa-twitter"></i>
                        </a>
                        <span class="noticia-integra-share-text">Gostou? Compartilhe</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 noticia-integra-text">
                        <p>Lidar com o luto de outra pessoa costuma ser bastante complicado, independente de quem seja. Para as empresas, é particularmente delicado lidar com o luto de um colega de trabalho, pois a queda de produtividade e socialização costumam diminuir bastante neste período.</p>

                        <p>Segundo Mariana Simonetti, psicóloga do Grupo Vila, o ideal seria que todas as empresas oferecessem um suporte psíquico ao colaborador, ou até mesmo um espaço de escuta. “Pequenas ações como um momento de conscientização com os funcionários de forma geral já é o suficiente para contribuir com a saúde e bem-estar deles”, afirma.A psicóloga também explica que é comum que o colaborador enlutado mude de comportamento e se afaste dos colegas de trabalho, o que pode gerar incompreensão e críticas, fazendo com que a pessoa sofra pelo luto e pela exclusão dos colegas.</p>

                        <p>“Por isso a necessidade de um acompanhamento e conscientização psicológica, para que não só a pessoa enlutada saiba lidar com os seus sentimentos, mas também todas as pessoas que estiverem ao redor dela”, completa. Dando essa abertura e construindo esse ambiente saudável, o enlutado se sente mais tranquilo. Mas Mariana alerta: oferecer ajuda é diferente de aceitar qualquer tipo de comportamento com o luto como justificativa. “A assistência deve existir, mas o funcionário não pode se sentir no direito de agir como quiser. É preciso haver compreensão das duas partes”, explica.</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="noticia-integra-subtitle">Leia também</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-lg-3 noticia-integra-item">
                    <a href="{{url('/noticias-integra')}}" title="Ler notícia">
                        <img src="{{asset('/images/noticias/apoio-familia.png')}}" alt="Apoio a família" class="noticia-integra-item-img">
                        <span class="noticia-integra-item-category">Apoio a família</span>
                        <h3 class="noticia-integra-item-title">Porque homenagear com flores?</h3>
                    </a>
                </div>
                <div class="col-md-6 col-lg-3 noticia-integra-item">
                    <a href="{{url('/noticias-integra')}}" title="Ler notícia">
                        <img src="{{asset('/images/noticias/vida.png')}}" alt="Vida" class="noticia-integra-item-img">
                        <span class="noticia-integra-item-category">Vida</span>
                        <h3 class="noticia-integra-item-title">Lidando com o luto de um colega de trabalho</h3>
                    </a>
                </div>
                <div class="col-md-6 col-lg-3 noticia-integra-item">
                    <a href="{{url('/noticias-integra')}}" title="Ler notícia">
                        <img src="{{asset('/images/noticias/familiares.png')}}" alt="Familiares" class="noticia-integra-item-img">
                        <span class="noticia-integra-item-category">Familiares</span>
                        <h3 class="noticia-integra-item-title">Diferentes formas de perder o medo da morte</h3>
                    </a>
                </div>
                <div class="col-md-6 col-lg-3 noticia-integra-item">
                    <a href="{{url('/noticias-integra')}}" title="Ler notícia">
                        <img src="{{asset('/images/noticias/dicas.png')}}" alt="Dicas" class="noticia-integra-item-img">
                        <span class="noticia-integra-item-category">Dicas</span>
                        <h3 class="noticia-integra-item-title">Porque homenagear com flores?</h3>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
