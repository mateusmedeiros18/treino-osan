@extends('layouts.app')

@section('title', 'Dúvidas')

@section('content')
    <div class="container page" id="page-faq">
        <div class="page-nav page-nav-wrapper mb-2 mb-lg-3">
            <div class="container">
                <div class="row">
                    <div class="col-12 pt-2 pt-lg-3">
                        <p class="page-nav-text">Você está em: <a href="{{url('/')}}" class="page-nav-link">Home</a> <a href="{{url('/faq')}}" class="page-nav-link page-nav-link-active">Dúvidas</a></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 pt-2 pt-lg-4 pb-2 pb-lg-5">
                        <h1 class="page-wrapper-title">Perguntas frequentes</h1>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <!-- Accordion FAQ -->
            <div class="accordion" id="faq">
                <!-- FAQ 1 -->
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <h3 class="mb-0">
                            <button class="btn btn-link accordion-title-btn" type="button" data-toggle="collapse" data-target="#faqOne" aria-expanded="true" aria-controls="faqOne">
                                <div class="accordion-title">
                                    <span class="accordion-title-number">1º</span> O que fazer em caso de falecimento?
                                </div>
                                <span class="accordion-title-icon">
                                    <i class="fas fa-chevron-right accordion-title-icon-first"></i>
                                    <i class="fas fa-chevron-down accordion-title-icon-second"></i>
                                </span>
                            </button>
                        </h3>
                    </div>

                    <div id="faqOne" class="collapse show" aria-labelledby="headingOne" data-parent="#faq">
                        <div class="card-body">
                            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Id deserunt cum magnam odit libero ab reprehenderit suscipit doloremque eaque minima dolorem, numquam praesentium aliquam ad officiis tempore veniam similique rerum quos, deleniti illum vel! Dolorem dolorum assumenda nemo molestias doloribus repellendus impedit, accusantium eos suscipit ad aspernatur nobis, soluta architecto?
                        </div>
                    </div>
                </div>

                <!-- FAQ 2 -->
                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <h3 class="mb-0">
                            <button class="btn btn-link accordion-title-btn" type="button" data-toggle="collapse" data-target="#faqTwo" aria-expanded="false" aria-controls="faqTwo">
                                <div class="accordion-title">
                                    <span class="accordion-title-number">2º</span> O que fazer caso o falecimento tenha ocorrido em casa?
                                </div>
                                <span class="accordion-title-icon">
                                    <i class="fas fa-chevron-right accordion-title-icon-first"></i>
                                    <i class="fas fa-chevron-down accordion-title-icon-second"></i>
                                </span>
                            </button>
                        </h3>
                    </div>
                    <div id="faqTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#faq">
                        <div class="card-body">
                            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Culpa nihil velit perspiciatis eum, adipisci a reprehenderit, ad distinctio mollitia doloremque corporis sit error ipsam illo. Cum molestiae blanditiis quo hic neque, tenetur tempore animi? Delectus, modi animi? Aut, culpa libero?
                        </div>
                    </div>
                </div>

                <!-- FAQ 3 -->
                <div class="card">
                    <div class="card-header" id="headingThree">
                        <h3 class="mb-0">
                            <button class="btn btn-link accordion-title-btn" type="button" data-toggle="collapse" data-target="#faqThree" aria-expanded="false" aria-controls="faqThree">
                                <div class="accordion-title">
                                    <span class="accordion-title-number">3º</span> Como lidar com o luto de um colega de trabalho?
                                </div>
                                <span class="accordion-title-icon">
                                    <i class="fas fa-chevron-right accordion-title-icon-first"></i>
                                    <i class="fas fa-chevron-down accordion-title-icon-second"></i>
                                </span>
                            </button>
                        </h3>
                    </div>
                    <div id="faqThree" class="collapse" aria-labelledby="headingThree" data-parent="#faq">
                        <div class="card-body">
                                A psicóloga também explica que é comum que o colaborador enlutado mude de comportamento e se afaste dos colegas de trabalho, o que pode gerar incompreensão e críticas, fazendo com que a pessoa sofra pelo luto e pela exclusão dos colegas.
                        </div>
                    </div>
                </div>

                <!-- FAQ 4 -->
                <div class="card">
                    <div class="card-header" id="headingFour">
                        <h3 class="mb-0">
                            <button class="btn btn-link accordion-title-btn" type="button" data-toggle="collapse" data-target="#faqFour" aria-expanded="false" aria-controls="faqFour">
                                <div class="accordion-title">
                                    <span class="accordion-title-number">4º</span> Quais os documentos para emissão da Certidão de Óbito?
                                </div>
                                <span class="accordion-title-icon">
                                    <i class="fas fa-chevron-right accordion-title-icon-first"></i>
                                    <i class="fas fa-chevron-down accordion-title-icon-second"></i>
                                </span>
                            </button>
                        </h3>
                    </div>
                    <div id="faqFour" class="collapse" aria-labelledby="headingFour" data-parent="#faq">
                        <div class="card-body">
                            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Culpa nihil velit perspiciatis eum, adipisci a reprehenderit, ad distinctio mollitia doloremque corporis sit error ipsam illo. Cum molestiae blanditiis quo hic neque, tenetur tempore animi? Delectus, modi animi? Aut, culpa libero?
                        </div>
                    </div>
                </div>

                <!-- FAQ 5 -->
                <div class="card">
                    <div class="card-header" id="headingFive">
                        <h3 class="mb-0">
                            <button class="btn btn-link accordion-title-btn" type="button" data-toggle="collapse" data-target="#faqFive" aria-expanded="false" aria-controls="faqFive">
                                <div class="accordion-title">
                                    <span class="accordion-title-number">5º</span> Preciso pagar taxas de Cemitério?
                                </div>
                                <span class="accordion-title-icon">
                                    <i class="fas fa-chevron-right accordion-title-icon-first"></i>
                                    <i class="fas fa-chevron-down accordion-title-icon-second"></i>
                                </span>
                            </button>
                        </h3>
                    </div>
                    <div id="faqFive" class="collapse" aria-labelledby="headingFive" data-parent="#faq">
                        <div class="card-body">
                            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Culpa nihil velit perspiciatis eum, adipisci a reprehenderit, ad distinctio mollitia doloremque corporis sit error ipsam illo. Cum molestiae blanditiis quo hic neque, tenetur tempore animi? Delectus, modi animi? Aut, culpa libero?
                        </div>
                    </div>
                </div>

                <!-- FAQ 6 -->
                <div class="card">
                    <div class="card-header" id="headingSix">
                        <h3 class="mb-0">
                            <button class="btn btn-link accordion-title-btn" type="button" data-toggle="collapse" data-target="#faqSix" aria-expanded="false" aria-controls="faqSix">
                                <div class="accordion-title">
                                    <span class="accordion-title-number">6º</span> Como funciona uma cremação?
                                </div>
                                <span class="accordion-title-icon">
                                    <i class="fas fa-chevron-right accordion-title-icon-first"></i>
                                    <i class="fas fa-chevron-down accordion-title-icon-second"></i>
                                </span>
                            </button>
                        </h3>
                    </div>
                    <div id="faqSix" class="collapse" aria-labelledby="headingSix" data-parent="#faq">
                        <div class="card-body">
                            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Culpa nihil velit perspiciatis eum, adipisci a reprehenderit, ad distinctio mollitia doloremque corporis sit error ipsam illo. Cum molestiae blanditiis quo hic neque, tenetur tempore animi? Delectus, modi animi? Aut, culpa libero?
                        </div>
                    </div>
                </div>

                <!-- FAQ 7 -->
                <div class="card">
                    <div class="card-header" id="headingSeven">
                        <h3 class="mb-0">
                            <button class="btn btn-link accordion-title-btn" type="button" data-toggle="collapse" data-target="#faqSeven" aria-expanded="false" aria-controls="faqSeven">
                                <div class="accordion-title">
                                    <span class="accordion-title-number">7º</span> Roteiro passo a passo para o velório, sepultamento ou cremação.
                                </div>
                                <span class="accordion-title-icon">
                                    <i class="fas fa-chevron-right accordion-title-icon-first"></i>
                                    <i class="fas fa-chevron-down accordion-title-icon-second"></i>
                                </span>
                            </button>
                        </h3>
                    </div>
                    <div id="faqSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#faq">
                        <div class="card-body">
                            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Culpa nihil velit perspiciatis eum, adipisci a reprehenderit, ad distinctio mollitia doloremque corporis sit error ipsam illo. Cum molestiae blanditiis quo hic neque, tenetur tempore animi? Delectus, modi animi? Aut, culpa libero?
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container pagination-container">
            <nav aria-label="Navegação das perguntas frequentes">
                <ul class="pagination justify-content-center">
                    <li class="page-item disabled">
                        <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Anterior</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Anterior">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Próximo">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" href="#">Próximo</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
@endsection
