@extends('layouts.app')

@section('title', 'Home')

@section('content')
    <!-- Slider -->
    <div class="slider">
        <div id="slider" class="carousel slide" data-ride="carousel">
            <!-- Indicadores slider -->
            <ol class="carousel-indicators">
                <li data-target="#slider" data-slide-to="0" class="active"></li>
                <li data-target="#slider" data-slide-to="1"></li>
                <li data-target="#slider" data-slide-to="2"></li>
            </ol>
            <!-- Items slider -->
            <div class="carousel-inner">
                <!-- Slide 1 -->
                <div class="carousel-item active" id="slide-1">
                    <div class="slider-legend">
                        <div class="carousel-caption slider-content">
                            <h1 class="slider-content-title">OSAN</h1>
                            <h2 class="slider-content-subtitle">Confira nossos planos</h2>
                            <p class="slider-content-text">O plano Osan é líder em assistência funeral. <br> É só ligar que cuidaremos de tudo para você.</p>
                            <a href="{{url('/contato')}}" title="Solicite um orçamento online com a Osan" class="btn btn-blue slider-content-btn">Orçamento online</a>
                        </div>
                    </div>
                </div>
                <!-- Slide 2 -->
                <div class="carousel-item" id="slide-2">
                    <div class="slider-legend">
                        <div class="carousel-caption slider-content">
                            <h1 class="slider-content-title">OSAN</h1>
                            <h2 class="slider-content-subtitle">Confira nossos planos</h2>
                            <p class="slider-content-text">O plano Osan é líder em assistência funeral. <br> É só ligar que cuidaremos de tudo para você.</p>
                            <a href="{{url('/contato')}}" title="Solicite um orçamento online com a Osan" class="btn btn-blue slider-content-btn">Orçamento online</a>
                        </div>
                    </div>
                </div>
                <!-- Slide 3 -->
                <div class="carousel-item" id="slide-3">
                    <div class="slider-legend">
                        <div class="carousel-caption slider-content">
                            <h1 class="slider-content-title">OSAN</h1>
                            <h2 class="slider-content-subtitle">Confira nossos planos</h2>
                            <p class="slider-content-text">O plano Osan é líder em assistência funeral. <br> É só ligar que cuidaremos de tudo para você.</p>
                            <a href="{{url('/contato')}}" title="Solicite um orçamento online com a Osan" class="btn btn-blue slider-content-btn">Orçamento online</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Controles slider -->
            <a class="carousel-control-prev" href="#slider" role="button" data-slide="prev">
                <div class="slider-control-container">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                </div>
                <span class="sr-only">Anterior</span>
            </a>
            <a class="carousel-control-next" href="#slider" role="button" data-slide="next">
                <div class="slider-control-container">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                </div>
                <span class="sr-only">Próximo</span>
            </a>
        </div>
    </div>

    <!-- Depoimentos -->
    <div class="depoimentos my-5 pb-4">
        <div class="container">
            <div class="row">
                <div class="col-12 section-title">
                    <h2 class="title">Depoimentos</h2>
                    <h3 class="subtitle">O que nossos clientes dizem</h3>
                </div>
            </div>

            <!-- Slider depoimentos -->
            <div class="row depoimentos-carousel">
                <!-- Item depoimentos -->
                <div class="col-12 col-lg-6 d-flex depoimentos-carousel-item">
                    <div class="col-lg-4 depoimentos-carousel-img">
                        <img src="{{asset('/images/icones/depoimento.png')}}">
                    </div>
                    <div class="col-lg-8 depoimentos-carousel-text">
                        <p class="depoimentos-carousel-text-msg">Muito obrigado por tudo. Você se mostrou exímia profissional, de muito tato e delicadeza, excelente energia. Tudo isso trouxe imenso alívio para a família.</p>
                        <p class="depoimentos-carousel-text-autor">- Deivid e família</p>
                    </div>
                </div>

                <!-- Item depoimentos -->
                <div class="col-12 col-lg-6 d-flex depoimentos-carousel-item">
                    <div class="col-lg-4 depoimentos-carousel-img">
                        <img src="{{asset('/images/icones/depoimento.png')}}">
                    </div>
                    <div class="col-lg-8 depoimentos-carousel-text">
                        <p class="depoimentos-carousel-text-msg">Muito obrigado por tudo. Você se mostrou exímia profissional, de muito tato e delicadeza, excelente energia. Tudo isso trouxe imenso alívio para a família.</p>
                        <p class="depoimentos-carousel-text-autor">- Lucas Evedove</p>
                    </div>
                </div>

                <!-- Item depoimentos -->
                <div class="col-12 col-lg-6 d-flex depoimentos-carousel-item">
                    <div class="col-lg-4 depoimentos-carousel-img">
                        <img src="{{asset('/images/icones/depoimento.png')}}">
                    </div>
                    <div class="col-lg-8 depoimentos-carousel-text">
                        <p class="depoimentos-carousel-text-msg">Muito obrigado por tudo. Você se mostrou exímia profissional, de muito tato e delicadeza, excelente energia. Tudo isso trouxe imenso alívio para a família.</p>
                        <p class="depoimentos-carousel-text-autor">- Lucas e família</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Notícias -->
    <div class="noticias my-5 pt-4">
        <div class="container">
            <div class="row">
                <div class="col-12 py-5 section-title section-title-secundary">
                    <h2 class="title">Notícias</h2>
                    <h3 class="subtitle">Confira nossas últimas notícias</h3>
                </div>
            </div>

            <div class="row noticias-container">
                <div class="container-fluid">
                    <div class="row">
                        <a href="{{url('/noticias-integra')}}" title="Ler a notícia" class="col-lg-6 noticias-item" id="noticia-1">
                            <h3 class="noticias-title">Programação para o dia da Lembrança 2018</h3>
                        </a>
                        <a href="{{url('/noticias-integra')}}" title="Ler a notícia" class="col-lg-6 noticias-item d-none d-lg-flex" id="noticia-2">
                            <h3 class="noticias-title">Meditação como auxílio de superação do luto</h3>
                        </a>
                    </div>
                    <div class="row">
                        <a href="{{url('/noticias-integra')}}" title="Ler a notícia" class="col-lg-4 noticias-item d-none d-lg-flex" id="noticia-3">
                            <h3 class="noticias-title">Saiba porque homenagear com flores?</h3>
                        </a>
                        <a href="{{url('/noticias-integra')}}" title="Ler a notícia" class="col-lg-4 noticias-item" id="noticia-4">
                            <h3 class="noticias-title">O que fazer com os pertences de um falecido?</h3>
                        </a>
                        <a href="{{url('/noticias-integra')}}" title="Ler a notícia" class="col-lg-4 noticias-item d-none d-lg-flex" id="noticia-5">
                            <h3 class="noticias-title">Qual a melhor forma de comunicar uma morte?</h3>
                        </a>
                    </div>
                </div>
            </div>

            <div class="row justify-content-center py-5 d-lg-none">
                <a href="{{url('/noticias')}}" title="Veja mais notícias da Osan" class="btn btn-blue">Outras notícias</a>
            </div>
        </div>
    </div>

    <!-- Serviços -->
    <div class="servicos my-5">
        <div class="container">
            <div class="row">
                <div class="col-12 py-5 section-title">
                    <h2 class="title">Serviços</h2>
                    <h3 class="subtitle">Conheçam os serviços que oferecemos</h3>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 col-lg-3 servicos-item">
                    <a href="{{url('/servicos-integra')}}" class="servicos-link" id="servico-1" title="Conferir serviço">
                        <img src="{{asset('/images/funeral.png')}}" alt="Serviço Funeral" class="d-none d-lg-block servicos-img">
                        <div class="servicos-info">
                            <h3 class="servicos-title">Serviço Funeral</h3>
                            <p class="servicos-desc">Lorem ipsum dolor sit amet, consect adipiscing elit Lorem ipsum dolor.</p>
                            <button type="button" class="btn btn-outline-blue">+ Saiba mais</button>
                        </div>
                    </a>
                </div>

                <div class="col-md-6 col-lg-3 servicos-item">
                    <a href="{{url('/servicos-integra')}}" class="servicos-link" id="servico-2" title="Conferir serviço">
                        <img src="{{asset('/images/translado-terrestre.png')}}" alt="Translado Terrestre" class="d-none d-lg-block servicos-img">
                        <div class="servicos-info">
                            <h3 class="servicos-title">Translado Terrestre</h3>
                            <p class="servicos-desc">Lorem ipsum dolor sit amet, consect adipiscing elit Lorem ipsum dolor.</p>
                            <button type="button" class="btn btn-outline-blue">+ Saiba mais</button>
                        </div>
                    </a>
                </div>

                <div class="col-md-6 col-lg-3 servicos-item">
                    <a href="{{url('/servicos-integra')}}" class="servicos-link" id="servico-3" title="Conferir serviço">
                        <img src="{{asset('/images/translado-aereo.png')}}" alt="Translado Aéreo" class="d-none d-lg-block servicos-img">
                        <div class="servicos-info">
                            <h3 class="servicos-title">Translado Aéreo</h3>
                            <p class="servicos-desc">Lorem ipsum dolor sit amet, consect adipiscing elit Lorem ipsum dolor.</p>
                            <button type="button" class="btn btn-outline-blue">+ Saiba mais</button>
                        </div>
                    </a>
                </div>

                <div class="col-md-6 col-lg-3 servicos-item">
                    <a href="{{url('/servicos-integra')}}" class="servicos-link" id="servico-4" title="Conferir serviço">
                        <img src="{{asset('/images/embalsamento.png')}}" alt="Embalsamento Formolização" class="d-none d-lg-block servicos-img">
                        <div class="servicos-info">
                            <h3 class="servicos-title">Embalsamento Formolização</h3>
                            <p class="servicos-desc">Lorem ipsum dolor sit amet, consect adipiscing elit Lorem ipsum dolor.</p>
                            <button type="button" class="btn btn-outline-blue">+ Saiba mais</button>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <!-- Unidades -->
    <div class="unidades">
        <div class="container">
            <div class="row">
                <div class="col-12 py-5 section-title">
                    <h2 class="title">Unidades</h2>
                    <h3 class="subtitle">Encontre a unidade mais próxima</h3>
                </div>
            </div>

            <div class="row">
                <div class="col-12 unidades-container">
                    <div class="row">
                        <!-- Unidade Santos -->
                        <div class="unidades-item col-md-6 col-lg-3">
                            <a href="https://goo.gl/maps/itEh3gzy9rQxRNU6A" title="Visualizar a unidade da Osan em Santos no mapa" class="unidades-link" id="unidade-1">
                                <h3 class="unidades-title"><i class="fas fa-map-marker-alt unidades-icon"></i> Santos</h3>
                            </a>
                        </div>

                        <!-- Unidade São Vicente -->
                        <div class="unidades-item col-md-6 col-lg-3">
                            <a href="https://goo.gl/maps/cR1uqG5S8Tmf2Mdi8" title="Visualizar a unidade da Osan em São Vicente no mapa" class="unidades-link" id="unidade-2">
                                <h3 class="unidades-title"><i class="fas fa-map-marker-alt unidades-icon"></i> São Vicente</h3>
                            </a>
                        </div>

                        <!-- Unidade Cubatão -->
                        <div class="unidades-item col-md-6 col-lg-3">
                            <a href="https://goo.gl/maps/P6mCfypoWdw1Wtzs9" title="Visualizar a unidade da Osan em Cubatão no mapa" class="unidades-link" id="unidade-3">
                                <h3 class="unidades-title"><i class="fas fa-map-marker-alt unidades-icon"></i> Cubatão</h3>
                            </a>
                        </div>

                        <!-- Unidade Praia Grande -->
                        <div class="unidades-item col-md-6 col-lg-3">
                            <a href="https://goo.gl/maps/72dMpjQRgXE2FjpZ9" title="Visualizar a unidade da Osan em Praia Grande no mapa" class="unidades-link" id="unidade-4">
                                <h3 class="unidades-title"><i class="fas fa-map-marker-alt unidades-icon"></i> Praia Grande</h3>
                            </a>
                        </div>
                    </div>
                    <div class="row d-none d-lg-flex">
                        <!-- Unidade Guarujá -->
                        <div class="unidades-item col-md-6 col-lg-3">
                            <a href="https://goo.gl/maps/ocfia8Py6wZZYNvp9" title="Visualizar a unidade da Osan em Guarujá no mapa" class="unidades-link" id="unidade-5">
                                <h3 class="unidades-title"><i class="fas fa-map-marker-alt unidades-icon"></i> Guarujá</h3>
                            </a>
                        </div>

                        <!-- Unidade Itanhaém -->
                        <div class="unidades-item col-md-6 col-lg-3">
                            <a href="https://goo.gl/maps/Ksq8weSgSpM3cQnV6" title="Visualizar a unidade da Osan em Itanhaém no mapa" class="unidades-link" id="unidade-6">
                                <h3 class="unidades-title"><i class="fas fa-map-marker-alt unidades-icon"></i> Itanhaém</h3>
                            </a>
                        </div>

                        <!-- Unidade Mongaguá -->
                        <div class="unidades-item col-md-6 col-lg-3">
                            <a href="https://goo.gl/maps/LokwMTmcrBMaH2Cw8" title="Visualizar a unidade da Osan em Mongaguá no mapa" class="unidades-link" id="unidade-7">
                                <h3 class="unidades-title"><i class="fas fa-map-marker-alt unidades-icon"></i> Mongaguá</h3>
                            </a>
                        </div>

                        <!-- Unidade Bertioga -->
                        <div class="unidades-item col-md-6 col-lg-3">
                            <a href="https://goo.gl/maps/Zs8dDMfQbDjNWYJP6" title="Visualizar a unidade da Osan em Bertioga no mapa" class="unidades-link" id="unidade-8">
                                <h3 class="unidades-title"><i class="fas fa-map-marker-alt unidades-icon"></i> Bertioga</h3>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
