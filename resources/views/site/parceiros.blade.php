@extends('layouts.app')

@section('title', 'Parceiros')

@section('content')
    <div class="container page" id="page-parceiros">
        <div class="page-nav page-nav-wrapper mb-2 mb-lg-3">
            <div class="container">
                <div class="row">
                    <div class="col-12 pt-2 pt-lg-3">
                        <p class="page-nav-text">Você está em: <a href="{{url('/')}}" class="page-nav-link">Home</a> <a href="{{url('/parceiros')}}" class="page-nav-link page-nav-link-active">Parceiros</a></p>
                    </div>
                </div>
                <div class="row pt-2 pt-lg-4 pb-2 pb-lg-5">
                    <div class="col-md-6">
                        <h1 class="page-wrapper-title">Parceiros</h1>
                    </div>
                    <div class="col-md-6 d-md-flex justify-content-end">
                        <div class="form-group page-input page-select">
                            <label for="filter" class="page-input-label">Buscar por:</label>
                            <select class="page-input-select" id="filter">
                                <option value="floricultura" selected>Floricultura</option>
                                <option value="cafeteria">Cafeteria</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container page-wrapper py-4 px-5 parceiros">
            <div class="row">
                <div class="col-md-4 parceiros-item">
                    <a href="{{url('/parceiros-integra')}}" title="Conferir Company">
                        <img src="{{asset('images/parceiros/logo-company.png')}}" alt="Logo da Company">
                        <div class="parceiros-content">
                            <h3 class="parceiros-title">Company</h3>
                            <a href="{{url('/parceiros-integra')}}" title="Visualizar mais informações da Company" class="parceiros-link">Mais Informações</a>
                        </div>
                    </a>
                </div>
                <div class="col-md-4 parceiros-item">
                    <a href="{{url('/parceiros-integra')}}" title="Conferir Autumn Leares">
                        <img src="{{asset('images/parceiros/logo-autumn.png')}}" alt="Logo da Autumn Leares">
                        <div class="parceiros-content">
                            <h3 class="parceiros-title">Autumn Leares</h3>
                            <a href="{{url('/parceiros-integra')}}" title="Visualizar mais informações da Autumn Leares" class="parceiros-link">Mais Informações</a>
                        </div>
                    </a>
                </div>
                <div class="col-md-4 parceiros-item">
                    <a href="{{url('/parceiros-integra')}}" title="Conferir Fryn Jewelry">
                        <img src="{{asset('images/parceiros/logo-fryn.png')}}" alt="Logo da Fryn Jewelry">
                        <div class="parceiros-content">
                            <h3 class="parceiros-title">Fryn Jewelry</h3>
                            <a href="{{url('/parceiros-integra')}}" title="Visualizar mais informações da Fryn Jewelry" class="parceiros-link">Mais Informações</a>
                        </div>
                    </a>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4 parceiros-item">
                    <a href="{{url('/parceiros-integra')}}" title="Conferir Beer Coffee">
                        <img src="{{asset('images/parceiros/logo-beer_coffee.png')}}" alt="Logo da Beer Coffee">
                        <div class="parceiros-content">
                            <h3 class="parceiros-title">Beer Coffee</h3>
                            <a href="{{url('/parceiros-integra')}}" title="Visualizar mais informações da Beer Coffee" class="parceiros-link">Mais Informações</a>
                        </div>
                    </a>
                </div>
                <div class="col-md-4 parceiros-item">
                    <a href="{{url('/parceiros-integra')}}" title="Conferir Bakery Company">
                        <img src="{{asset('images/parceiros/logo-bakery.png')}}" alt="Logo da Bakery Company">
                        <div class="parceiros-content">
                            <h3 class="parceiros-title">Bakery Company</h3>
                            <a href="{{url('/parceiros-integra')}}" title="Visualizar mais informações da Bakery Company" class="parceiros-link">Mais Informações</a>
                        </div>
                    </a>
                </div>
                <div class="col-md-4 parceiros-item">
                    <a href="{{url('/parceiros-integra')}}" title="Conferir Ousada Flor">
                        <img src="{{asset('images/parceiros/logo-ousada_flor.png')}}" alt="Logo da Ousada Flor">
                        <div class="parceiros-content">
                            <h3 class="parceiros-title">Ousada Flor</h3>
                            <a href="{{url('/parceiros-integra')}}" title="Visualizar mais informações da Ousada Flor" class="parceiros-link">Mais Informações</a>
                        </div>
                    </a>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4 parceiros-item">
                    <a href="{{url('/parceiros-integra')}}" title="Conferir Mult Flores">
                        <img src="{{asset('images/parceiros/logo-mult_flores.png')}}" alt="Logo da Mult Flores">
                        <div class="parceiros-content">
                            <h3 class="parceiros-title">Mult Flores</h3>
                            <a href="{{url('/parceiros-integra')}}" title="Visualizar mais informações da Mult Flores" class="parceiros-link">Mais Informações</a>
                        </div>
                    </a>
                </div>
                <div class="col-md-4 parceiros-item">
                    <a href="{{url('/parceiros-integra')}}" title="Conferir Gardeh Sul">
                        <img src="{{asset('images/parceiros/logo-gardeh_sul.png')}}" alt="Logo da Gardeh Sul">
                        <div class="parceiros-content">
                            <h3 class="parceiros-title">Gardeh Sul</h3>
                            <a href="{{url('/parceiros-integra')}}" title="Visualizar mais informações da Gardeh Sul" class="parceiros-link">Mais Informações</a>
                        </div>
                    </a>
                </div>
                <div class="col-md-4 parceiros-item">
                    <a href="{{url('/parceiros-integra')}}" title="Conferir Faça Bonito">
                        <img src="{{asset('images/parceiros/logo-faca_bonito.png')}}" alt="Logo da Faça Bonito">
                        <div class="parceiros-content">
                            <h3 class="parceiros-title">Faça Bonito</h3>
                            <a href="{{url('/parceiros-integra')}}" title="Visualizar mais informações da Faça Bonito" class="parceiros-link">+ Mais Informações</a>
                        </div>
                    </a>
                </div>
            </div>
        </div>

        <div class="container pagination-container">
            <nav aria-label="Navegação das perguntas frequentes">
                <ul class="pagination justify-content-center">
                    <li class="page-item disabled">
                        <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Anterior</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Anterior">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Próximo">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" href="#">Próximo</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
@endsection
