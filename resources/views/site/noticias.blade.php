@extends('layouts.app')

@section('title', 'Notícias')

@section('content')
    <div class="container page" id="page-noticias">
        <div class="page-nav page-nav-wrapper mb-2 mb-lg-3">
            <div class="container">
                <div class="row">
                    <div class="col-12 pt-2 pt-lg-3">
                        <p class="page-nav-text">Você está em: <a href="{{url('/')}}" class="page-nav-link">Home</a> <a href="{{url('/noticias')}}" class="page-nav-link page-nav-link-active">Notícias</a></p>
                    </div>
                </div>
                <div class="row pt-2 pt-lg-4 pb-2 pb-lg-5">
                    <div class="col-md-6">
                        <h1 class="page-wrapper-title">Notícias</h1>
                    </div>
                    <div class="col-md-6 d-md-flex justify-content-end">
                        <div class="form-group page-input w-100">
                            <input class="form-control page-input-search" type="search" placeholder="Buscar por:" aria-label="Pesquisar">
                            <button class="btn page-input-search-btn" type="submit"><i class="fas fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container page-wrapper pt-5 pb-4 px-5">
            <div class="row">
                <div class="col-md-4 noticia-item">
                    <a href="{{url('/noticias-integra')}}" title="Ler notícia">
                        <img src="{{asset('images/noticias/dia-lembranca.png')}}" class="img-fluid" alt="Programação para o dia da lembrança 2017">
                        <div class="noticia-content">
                            <time class="noticia-date" datetime=”2017-05-17”>17 de maio de 2017</time>
                            <h3 class="noticia-title">Programação para o dia da lembrança 2017</h3>
                        </div>
                        <a href="{{url('/noticias-integra')}}" class="btn btn-outline-blue noticia-link" title="Ler notícia completa">+ Ler mais</a>
                    </a>
                </div>
                <div class="col-md-4 noticia-item">
                    <a href="{{url('/noticias-integra')}}" title="Ler notícia">
                        <img src="{{asset('images/noticias/meditacao-luto.png')}}" class="img-fluid" alt="Meditação como auxílio de superação de luto">
                        <div class="noticia-content">
                            <time class="noticia-date" datetime=”2017-05-17”>17 de maio de 2017</time>
                            <h3 class="noticia-title">Meditação como auxílio de superação de luto</h3>
                        </div>
                        <a href="{{url('/noticias-integra')}}" class="btn btn-outline-blue noticia-link" title="Ler notícia completa">+ Ler mais</a>
                    </a>
                </div>
                <div class="col-md-4 noticia-item">
                    <a href="{{url('/noticias-integra')}}" title="Ler notícia">
                        <img src="{{asset('images/noticias/pertences-falecidos.png')}}" class="img-fluid" alt="O que fazer com os pertences de um falecido">
                        <div class="noticia-content">
                            <time class="noticia-date" datetime=”2017-05-17”>17 de maio de 2017</time>
                            <h3 class="noticia-title">O que fazer com os pertences de um falecido?</h3>
                        </div>
                        <a href="{{url('/noticias-integra')}}" class="btn btn-outline-blue noticia-link" title="Ler notícia completa">+ Ler mais</a>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 noticia-item">
                    <a href="{{url('/noticias-integra')}}" title="Ler notícia">
                        <img src="{{asset('images/noticias/homenagem-flores.png')}}" class="img-fluid" alt="Porque homenagear com flores">
                        <div class="noticia-content">
                            <time class="noticia-date" datetime=”2017-05-17”>17 de maio de 2017</time>
                            <h3 class="noticia-title">Porque homenagear com flores?</h3>
                        </div>
                        <a href="{{url('/noticias-integra')}}" class="btn btn-outline-blue noticia-link" title="Ler notícia completa">+ Ler mais</a>
                    </a>
                </div>
                <div class="col-md-4 noticia-item">
                    <a href="{{url('/noticias-integra')}}" title="Ler notícia">
                        <img src="{{asset('images/noticias/luto-colega.png')}}" class="img-fluid" alt="Lidando com o luto de um colega de trabalho">
                        <div class="noticia-content">
                            <time class="noticia-date" datetime=”2017-05-17”>17 de maio de 2017</time>
                            <h3 class="noticia-title">Lidando com o luto de um colega de trabalho</h3>
                        </div>
                        <a href="{{url('/noticias-integra')}}" class="btn btn-outline-blue noticia-link" title="Ler notícia completa">+ Ler mais</a>
                    </a>
                </div>
                <div class="col-md-4 noticia-item">
                    <a href="{{url('/noticias-integra')}}" title="Ler notícia">
                        <img src="{{asset('images/noticias/medo-morte.png')}}" class="img-fluid" alt="Diferentes formas de perder o medo da morte">
                        <div class="noticia-content">
                            <time class="noticia-date" datetime=”2017-05-17”>17 de maio de 2017</time>
                            <h3 class="noticia-title">Diferentes formas de perder o medo da morte</h3>
                        </div>
                        <a href="{{url('/noticias-integra')}}" class="btn btn-outline-blue noticia-link" title="Ler notícia completa">+ Ler mais</a>
                    </a>
                </div>
            </div>
        </div>

        <div class="container pagination-container">
            <nav aria-label="Navegação das perguntas frequentes">
                <ul class="pagination justify-content-center">
                    <li class="page-item disabled">
                        <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Anterior</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Anterior">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Próximo">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" href="#">Próximo</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
@endsection
