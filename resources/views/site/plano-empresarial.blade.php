@extends('layouts.app')

@section('title', 'Plano Empresarial')

@section('content')

    <div class="page-nav mt-2 mt-lg-3 mb-2 mb-lg-5">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <p class="page-nav-text">Você está em: <a href="{{url('/')}}" class="page-nav-link">Home</a> <a href="{{url('/plano-empresarial')}}" class="page-nav-link page-nav-link-active">Plano Empresarial</a></p>
                </div>
            </div>
        </div>
    </div>

    <div class="page-destaque">
        <div class="container">
            <div class="row page-destaque-container page-destaque-container-secundary">
                <div class="col-12 col-md-6 page-destaque-title-container">
                    <h1 class="page-destaque-title">Plano <span class="page-destaque-title-subline">Empresarial</span></h1>
                    <h2 class="page-destaque-subtitle">De assistência funeral</h2>
                </div>
                <div class="col-12 col-md-6 page-destaque-img-container">
                    <img src="{{asset('images/plano-empresarial.png')}}" alt="Plano empresarial da Osan" class="img-fluid page-destaque-img">
                </div>
            </div>
        </div>
    </div>

    <div class="page-content my-md-4">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6">
                    <section class="page-content-section">
                        <p class="page-content-text">
                            Para atender ao crescente interesse por parte de empresas privadas, sindicatos e associações, a OSAN desenvolveu o <strong>Plano Empresarial de Assistência Funeral</strong>, que tem o objetivo de minimizar os transtornos e as responsabilidades quando ocorre um óbito de um colaborador ou associado.
                        </p>
                    </section>

                    <!-- Dependentes -->
                    <section class="page-content-section">
                        <h2 class="page-content-title">
                            <img src="{{asset('images/icones/dependentes.png')}}" class="page-content-title-icon">
                            Dependentes
                        </h2>
                        <p class="page-content-text">
                            O Plano Empresarial de Assistência Funeral permite ao titular incluir como dependentes: cônjuge ou companheira (o), pais, sogros, filhos solteiros e outros dependentes oficialmente comprovados, desde que sejam autorizados pela entidade. Os serviços serão prestados a todo e qualquer usuário, independentemente de faixa etária, doenças pré-existentes ou município de residência que fizerem a devida adesão.

                        </p>
                    </section>

                    <!-- Pagamento -->
                    <section class="page-content-section">
                        <h2 class="page-content-title">
                            <img src="{{asset('images/icones/pagamento.png')}}" class="page-content-title-icon">
                            Forma de pagamento
                        </h2>
                        <p class="page-content-text">
                            Consulte-nos.
                        </p>
                    </section>

                    <!-- Central de atendimento -->
                    <section class="page-content-section">
                        <h2 class="page-content-title">
                            <img src="{{asset('images/icones/central-atendimento.png')}}" class="page-content-title-icon">
                            Central de Atendimento 24 horas
                        </h2>
                        <p class="page-content-text">
                            A <strong>OSAN</strong> disponibiliza aos associados uma Central de Atendimento 24 horas, que oferece informações sobre procedimentos funerários, recepciona comunicados de falecimento, toma providências referentes ao cerimonial e assessora a família durante todo o serviço.
                        </p>
                        <p class="page-content-text page-content-text-special">
                            <strong>Em caso de falecimento, ligue gratuitamente para 0800 017 8000.</strong>
                        </p>
                    </section>

                    <!-- Departamento Empresarial -->
                    <section class="page-content-section">
                        <h2 class="page-content-title">
                            <img src="{{asset('images/icones/departamento-empresarial.png')}}" class="page-content-title-icon">
                            Departamento do Plano Empresarial de Assistência Funeral
                        </h2>
                        <p class="page-content-text">
                            O Departamento do Plano Empresarial de Assistência Funeral realiza atendimento exclusivo para as empresas. Diariamente, são enviados relatórios, por e-mail, contendo informações sobre cadastro dos associados.
                        </p>
                        <p class="page-content-text">
                            Além disso, é possível esclarecer dúvidas sobre faturas, movimentação de vidas e 2ª via de boleto através do telefone (13) 3228 8019 ou e-mail <a href="mailto:planoempresa@osan.com.br" title="Enviar email para Plano Empresarial da Osan" class="page-content-text-link">planoempresa@osan.com.br</a>
                        </p>
                    </section>
                </div>
                <div class="col-12 col-lg-6">
                    <!-- Coberturas -->
                    <section class="page-content-section">
                        <h2 class="page-content-title">
                            <img src="{{asset('images/icones/coberturas.png')}}" class="page-content-title-icon">
                            Coberturas
                        </h2>
                        <ul class="page-content-list">
                                <li class="page-content-list-item">Urna sextavada, envernizada com alça varão e visor;</li>
                                <li class="page-content-list-item">Ornamentação;</li>
                                <li class="page-content-list-item">Paramentos para velório;</li>
                                <li class="page-content-list-item">Livro de Presença;</li>
                                <li class="page-content-list-item">Véu para urna;</li>
                                <li class="page-content-list-item">Velas próprias para velório;</li>
                                <li class="page-content-list-item">Uma Coroa de Flores (1 Bola);</li>
                                <li class="page-content-list-item">Higienização do corpo;</li>
                                <li class="page-content-list-item">Declaração de óbito;</li>
                                <li class="page-content-list-item">Guia de sepultamento;</li>
                                <li class="page-content-list-item">Taxa de Sepultamento em Cemitério Municipal no município da última residência do falecido;</li>
                                <li class="page-content-list-item">Agendamento junto ao Cemitério;</li>
                                <li class="page-content-list-item">Veículos para remoção e/ou cortejo dentro da abrangência geográfica;</li>
                                <li class="page-content-list-item">Veículos para translados com franquia de 200 km percorridos (ida e volta);</li>
                                <li class="page-content-list-item">Serviço de café e biscoito nos primeiros momentos do velório, somente nas seguintes regiões: Itanhaém, Mongaguá, Praia Grande, São Vicente, Cubatão, Santos, Guarujá e Bertioga;</li>
                                <li class="page-content-list-item">Velório dentro da abrangência geográfica e de acordo com a disponibilidade do dia e hora nos Cemitérios Municipais;</li>
                                <li class="page-content-list-item">Velório particular sob denominação Esmeralda na instalação da <strong>OSAN</strong> em Praia Grande de acordo com a disponibilidade do dia e hora.</li>
                        </ul>
                    </section>

                    <!-- Transporte -->
                    <section class="page-content-section">
                        <h2 class="page-content-title">
                            <img src="{{asset('images/icones/transporte.png')}}" class="page-content-title-icon">
                            Transporte
                        </h2>
                        <p class="page-content-text">
                            A frota da <strong>OSAN</strong> contém mais de 20 automóveis  que estão disponíveis para servir o associado, além de veículos exclusivos para cortejos fúnebres realizados pela empresa na Baixada Santista.
                        </p>
                    </section>
                </div>
            </div>

            <div class="row my-4 pb-5">
                <div class="col-12">
                    <a href="#" title="Contratar plano empresarial da Osan" class="btn btn-blue-dark page-content-btn">Contratar esse plano</a>
                    <a href="{{url('/plano-classico')}}" title="Conhecer plano clássico da Osan" class="btn btn-outline-secundary page-content-btn">Conheça também o plano clássico</a>
                </div>
            </div>
        </div>
    </div>


@endsection
