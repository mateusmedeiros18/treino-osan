@extends('layouts.app')

@section('title', 'A Osan')

@section('content')
    <div class="container page" id="page-depoimentos">
        <div class="page-nav page-nav-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-12 pt-2 pt-lg-3">
                        <p class="page-nav-text">Você está em: <a href="{{url('/')}}" class="page-nav-link">Home</a> <a href="{{url('/sobre')}}" class="page-nav-link page-nav-link-active">A Osan</a></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 pt-2 pt-lg-4 pb-2 pb-lg-5">
                        <h1 class="page-wrapper-title">A Osan</h1>
                    </div>
                </div>
            </div>
        </div>

        <div class="container page-wrapper pt-5 pb-4 px-4 px-md-5">
            <div class="row">
                <div class="col-12">
                    <h2 class="mb-4 page-wrapper-subtitle">OSAN, Sinônimo de Bons Serviços Prestados à Baixada Santista.</h2>
                    <p class="page-wrapper-text">A Organização Social de Ataúdes Nóvoa – OSAN, atualmente está consolidada e é reconhecida por profissionais capacitados que humanizam o atendimento. A sua história é marcada por muito trabalho, dedicação e respeito aos milhares de associados. Os fundadores da OSAN, Sr. Manoel Rodriguez Nóvoa e sua esposa Marina Gonzalez Lopez, naturais da Espanha, inauguraram em 1960 uma pequena fábrica de urnas. Em 1965, o espírito de luta de seus fundadores transformou a pequena fábrica em um empreendimento próspero, bem organizado e totalmente dirigido à família: a Organização Social de Ataúdes Nóvoa.</p>
                    <p class="page-wrapper-text">O casal e seu único filho, Manoel Rodriguez Gonzalez, carinhosamente chamado de Manolo, dirigiam a empresa que expandia suas atividades, iniciadas em Santos, para outros municípios como Praia Grande, São Vicente, Mongaguá, Itanhaém, Cubatão e Guarujá.</p>
                    <p class="page-wrapper-text">No início dos anos 90 começa uma nova etapa para os negócios da família. Manolo e sua esposa Cátia, assumem a administração da empresa. Surge, então, o Plano de Assistência familiar OSAN.</p>
                    <p class="page-wrapper-text">Nesta época, a empresa sofre uma reestruturação física e as unidades são informatizadas, a frota de veículos totalmente renovada e a filosofia organizacional repensada para atender, ainda melhor, aos seus clientes. Outros pontos de atendimento foram inaugurados, o que gerou muitos empregos diretos e indiretos.</p>
                    <p class="page-wrapper-text">Atualmente, a marca OSAN está consolidada e é reconhecida pela seriedade e qualidade dos serviços que presta. Prova disso é o mais de meio milhão de associados ao Plano de Assistência Familiar, que conta com uma equipe formada por profissionais capacitados que humanizam o atendimento, em respeito a você.</p>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-md-4 mb-3 mb-md-0">
                    <h2 class="mb-4 page-wrapper-subtitle">Missão</h2>
                    <p class="page-wrapper-text">Proporcionar aos nossos clientes uma assistência funeral digna e humanizada, minimizando os impactos financeiros e superando suas expectativas.</p>
                </div>
                <div class="col-md-4 mb-3 mb-md-0">
                    <h2 class="mb-4 page-wrapper-subtitle">Visão</h2>
                    <p class="page-wrapper-text">Tornar-se referência nacional no segmento em que atua, sendo reconhecida por associados e colaboradores, como uma empresa eficaz na gestão de assistência funeral.</p>
                </div>
                <div class="col-md-4">
                    <h2 class="mb-4 page-wrapper-subtitle">Valores</h2>
                    <ul>
                        <li class="page-wrapper-text">Eficiência</li>
                        <li class="page-wrapper-text">Excelência</li>
                        <li class="page-wrapper-text">Disciplina</li>
                        <li class="page-wrapper-text">Cooperação</li>
                        <li class="page-wrapper-text">Humanização</li>
                        <li class="page-wrapper-text">Reconhecimento</li>
                        <li class="page-wrapper-text">Sustentabilidade econômica</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
