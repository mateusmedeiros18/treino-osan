@extends('layouts.app')

@section('title', 'Plano Clássico')

@section('content')

    <div class="page-nav mt-2 mt-lg-3 mb-2 mb-lg-5">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <p class="page-nav-text">Você está em: <a href="{{url('/')}}" class="page-nav-link">Home</a> <a href="{{url('/plano-classico')}}" class="page-nav-link page-nav-link-active">Plano Clássico</a></p>
                </div>
            </div>
        </div>
    </div>

    <div class="page-destaque">
        <div class="container">
            <div class="row page-destaque-container">
                <div class="col-12 col-md-6 page-destaque-title-container">
                    <h1 class="page-destaque-title">Plano <span class="page-destaque-title-subline">Clássico</span></h1>
                    <h2 class="page-destaque-subtitle">De assistência funeral</h2>
                </div>
                <div class="col-12 col-md-6 page-destaque-img-container">
                    <img src="{{asset('images/plano-classico.png')}}" alt="Plano clássico da Osan" class="img-fluid page-destaque-img">
                </div>
            </div>
        </div>
    </div>

    <div class="page-content my-md-4">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6">
                    <section class="page-content-section">
                        <p class="page-content-text">
                            Criado para minimizar desconfortos, o <strong>Plano de Assistência Funeral</strong> oferece tranquilidade e evita a burocracia e os transtornos financeiros pertinentes à organização de um funeral.
                        </p>
                        <p class="page-content-text">
                            Através de atendimento humanizado, a <strong>OSAN</strong> proporciona segurança às famílias enlutadas, em caso de falecimento de ente querido. Dessa forma, você e sua família não precisarão se preocupar com nada, pois a <strong>OSAN</strong> realizará todos os procedimentos funerários necessários.
                        </p>
                    </section>

                    <!-- Dependentes -->
                    <section class="page-content-section">
                        <h2 class="page-content-title">
                            <img src="{{asset('images/icones/dependentes.png')}}" class="page-content-title-icon">
                            Dependentes
                        </h2>
                        <p class="page-content-text">
                            Cada titular do Plano de Assistência Funeral tem direito a inscrever como dependentes: cônjuge ou companheira (o), pai, mãe, sogro, sogra e filhos solteiros.
                        </p>
                    </section>

                    <!-- Dependentes Adicionais -->
                    <section class="page-content-section">
                        <h2 class="page-content-title">
                            <img src="{{asset('images/icones/dependentes-adicionais.png')}}" class="page-content-title-icon">
                            Dependentes Adicionais
                        </h2>
                        <p class="page-content-text">
                            É possível incluir até dois dependentes adicionais no plano, independente do grau de parentesco. Para isso, haverá acréscimo de R$ 12,00 no valor da mensalidade de cada dependente.
                        </p>
                    </section>

                    <!-- Pagamento -->
                    <section class="page-content-section">
                        <h2 class="page-content-title">
                            <img src="{{asset('images/icones/pagamento.png')}}" class="page-content-title-icon">
                            Prazo e forma de pagamento
                        </h2>
                        <p class="page-content-text">
                            A contratação do Plano de Assistência Funeral terá vigência de quatro anos, podendo ser renovada por igual período.
                        </p>
                        <p class="page-content-text">
                            A mensalidade do plano será de R$ 42,90, sendo a primeira a ser paga no ato da venda. A partir da segunda parcela, o titular do plano poderá escolher pagar dia 10 ou 22 de cada mês.
                        </p>
                    </section>

                    <!-- Central de atendimento -->
                    <section class="page-content-section">
                        <h2 class="page-content-title">
                            <img src="{{asset('images/icones/central-atendimento.png')}}" class="page-content-title-icon">
                            Central de Atendimento 24 horas
                        </h2>
                        <p class="page-content-text">
                            A OSAN disponibiliza aos associados uma Central de Atendimento 24 horas, que oferece informações sobre procedimentos funerários, recepciona comunicados de falecimento, toma providências referentes ao cerimonial e assessora a família durante todo o serviço.
                        </p>
                        <p class="page-content-text page-content-text-special">
                            <strong>Em caso de falecimento, ligue gratuitamente para 0800 017 8000.</strong>
                        </p>
                    </section>
                </div>
                <div class="col-12 col-lg-6">
                    <!-- Coberturas -->
                    <section class="page-content-section">
                        <h2 class="page-content-title">
                            <img src="{{asset('images/icones/coberturas.png')}}" class="page-content-title-icon">
                            Coberturas
                        </h2>
                        <ul class="page-content-list">
                                <li class="page-content-list-item">Urna sextavada, envernizada com alça varão e visor;</li>
                                <li class="page-content-list-item">Ornamentação;</li>
                                <li class="page-content-list-item">Paramentos para velório;</li>
                                <li class="page-content-list-item">Livro de Presença;</li>
                                <li class="page-content-list-item">Véu para urna;</li>
                                <li class="page-content-list-item">Velas próprias para velório;</li>
                                <li class="page-content-list-item">Uma Coroa de Flores (1 Bola);</li>
                                <li class="page-content-list-item">Higienização do corpo;</li>
                                <li class="page-content-list-item">Declaração de óbito;</li>
                                <li class="page-content-list-item">Guia de sepultamento;</li>
                                <li class="page-content-list-item">Agendamento junto ao Cemitério;</li>
                                <li class="page-content-list-item">Veículos para remoção e/ou cortejo dentro da abrangência geográfica;</li>
                                <li class="page-content-list-item">Veículos para translados com franquia de 200 km percorridos (ida e volta);</li>
                                <li class="page-content-list-item">Serviço de café e biscoito nos primeiros momentos do velório;</li>
                                <li class="page-content-list-item">Velório dentro da abrangência geográfica e de acordo com a disponibilidade do dia e hora nos Cemitérios Municipais;</li>
                        </ul>
                    </section>

                    <!-- Carência -->
                    <section class="page-content-section">
                        <h2 class="page-content-title">
                            <img src="{{asset('images/icones/carencia.png')}}" class="page-content-title-icon">
                            Carência
                        </h2>
                        <p class="page-content-text">
                            Noventa dias após a data da venda do plano, desde que o contratante esteja com o pagamento em dia das parcelas mensais.
                        </p>
                    </section>

                    <!-- Transporte -->
                    <section class="page-content-section">
                        <h2 class="page-content-title">
                            <img src="{{asset('images/icones/transporte.png')}}" class="page-content-title-icon">
                            Transporte
                        </h2>
                        <p class="page-content-text">
                            A <strong>OSAN</strong> possui ampla frota com mais de 20 automóveis disponíveis para servir o associado, além de veículos exclusivos para cortejos fúnebres realizados pela empresa na Baixada Santista.
                        </p>
                    </section>
                </div>
            </div>

            <div class="row my-4 pb-5">
                <div class="col-12">
                    <a href="#" title="Contratar plano clássico da Osan" class="btn btn-blue-dark page-content-btn">Contratar esse plano</a>
                    <a href="{{url('/plano-empresarial')}}" title="Conhecer plano empresarial da Osan" class="btn btn-outline-secundary page-content-btn">Conheça também o plano empresarial</a>
                </div>
            </div>
        </div>
    </div>


@endsection
