@extends('layouts.app')

@section('title', 'Parceiros Íntegra')

@section('content')
    <div class="container page" id="page-parceiros">
        <div class="page-nav page-nav-wrapper mb-2 mb-lg-3">
            <div class="container">
                <div class="row">
                    <div class="col-12 pt-2 pt-lg-3">
                        <p class="page-nav-text">Você está em: <a href="{{url('/')}}" class="page-nav-link">Home</a> <a href="{{url('/parceiros')}}" class="page-nav-link page-nav-link-active">Parceiros</a></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 pt-2 pt-lg-4 pb-2 pb-lg-5">
                        <h1 class="page-wrapper-title">Parceiros</h1>
                    </div>
                </div>
            </div>
        </div>

        <div class="container page-wrapper py-4 px-5">
            <div class="row">
                <div class="col-md-3 parceiros-integra-img">
                    <img src="{{asset('images/parceiros/logo-bakery.png')}}" alt="Logo da Bakery Company">
                </div>
                <div class="col-md-9 pl-md-5 parceiros-integra-content">
                    <h3 class="parceiros-integra-title">Bakery Company</h3>
                    <p class="parceiros-integra-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsam quisquam maiores eum nobis, asperiores aliquid porro libero voluptates ut aspernatur commodi expedita ex, laboriosam, repudiandae qui a aliquam minus vero.</p>
                    <p class="parceiros-integra-text">Repudiandae adipisci, ut laudantium rem sit voluptatem cum nisi voluptate, ab quae dolorem, nobis quod accusamus? Rem ducimus sunt culpa ipsa dolor in quae odio exercitationem non unde. Voluptas omnis quod iure itaque unde enim illum nihil quae quos incidunt perferendis, voluptatem magni praesentium, placeat suscipit accusantium est!</p>
                </div>
            </div>
        </div>

        <div class="container mt-4 mt-md-5">
            <div class="row pt-4">
                <div class="col-12">
                    <h2 class="parceiros-integra-subtitle">Veja outros parceiros</h2>
                </div>
            </div>
            <div class="row mt-md-3 mb-md-5 parceiros-integra">
                <div class="col-md-6 col-lg-3 parceiros-item">
                    <a href="{{url('/parceiros-integra')}}" title="Conferir Beer Coffee">
                        <img src="{{asset('images/parceiros/logo-beer_coffee.png')}}" alt="Logo da Beer Coffee">
                        <div class="parceiros-content">
                            <h3 class="parceiros-title">Beer Coffee</h3>
                            <a href="{{url('/parceiros-integra')}}" title="Visualizar mais informações da Beer Coffee" class="parceiros-link">Mais Informações</a>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-3 parceiros-item">
                    <a href="{{url('/parceiros-integra')}}" title="Conferir Company">
                        <img src="{{asset('images/parceiros/logo-company.png')}}" alt="Logo da Company">
                        <div class="parceiros-content">
                            <h3 class="parceiros-title">Company</h3>
                            <a href="{{url('/parceiros-integra')}}" title="Visualizar mais informações da Company" class="parceiros-link">Mais Informações</a>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-3 parceiros-item">
                    <a href="{{url('/parceiros-integra')}}" title="Conferir Ousada Flor">
                        <img src="{{asset('images/parceiros/logo-ousada_flor.png')}}" alt="Logo da Ousada Flor">
                        <div class="parceiros-content">
                            <h3 class="parceiros-title">Ousada Flor</h3>
                            <a href="{{url('/parceiros-integra')}}" title="Visualizar mais informações da Ousada Flor" class="parceiros-link">Mais Informações</a>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-lg-3 parceiros-item">
                    <a href="{{url('/parceiros-integra')}}" title="Conferir Mult Flores">
                        <img src="{{asset('images/parceiros/logo-mult_flores.png')}}" alt="Logo da Mult Flores">
                        <div class="parceiros-content">
                            <h3 class="parceiros-title">Mult Flores</h3>
                            <a href="{{url('/parceiros-integra')}}" title="Visualizar mais informações da Mult Flores" class="parceiros-link">Mais Informações</a>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
