@extends('layouts.app')

@section('title', 'Contatos')

@section('content')
    <div class="container page contato" id="page-contato">
        <div class="page-nav page-nav-wrapper mb-2 mb-lg-3">
            <div class="container">
                <div class="row">
                    <div class="col-12 pt-2 pt-lg-3">
                        <p class="page-nav-text">Você está em: <a href="{{url('/')}}" class="page-nav-link">Home</a> <a href="{{url('/contato')}}" class="page-nav-link page-nav-link-active">Contatos</a></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 pt-2 pt-lg-4 pb-2 pb-lg-5">
                        <h1 class="page-wrapper-title">Contatos</h1>
                    </div>
                </div>
            </div>
        </div>

        <div class="container page-wrapper py-5 px-5">
            <div class="row">
                <div class="col-md-6">
                    <p class="mt-lg-5 form-description">Em caso de dúvidas, informações ou sugestões, nos envie uma mensagem através do formulário abaixo.</p>

                    <form class="form needs-validation" novalidate>
                        <div class="form-group">
                            <label for="name"><sup>*</sup>Nome</label>
                            <input type="text" class="form-control" id="name" required>
                            <div class="invalid-feedback">
                                Por favor, preencha seu nome corretamente!
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email"><sup>*</sup>Email</label>
                            <input type="email" class="form-control" id="email" required>
                            <div class="invalid-feedback">
                                Por favor, preencha seu email corretamente!
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 form-group">
                                <label for="tel"><sup>*</sup>Telefone</label>
                                <input type="tel" class="form-control" id="tel" required>
                                <div class="invalid-feedback">
                                    Por favor, preencha seu telefone corretamente!
                                </div>
                            </div>
                            <div class="col-lg-6 form-group">
                                <label for="dep"><sup>*</sup>Departamento</label>
                                <select class="form-control" id="dep" required>
                                    <option value="depVenda">Departamento de vendas</option>
                                    <option value="depCobranca">Departamento de cobrança</option>
                                    <option value="depFaturamento">Departamento de faturamento</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="assunto"><sup>*</sup>Assunto</label>
                            <input type="text" class="form-control" id="assunto" required>
                            <div class="invalid-feedback">
                                Por favor, especifique o assunto!
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="msg"><sup>*</sup>Mensagem</label>
                            <textarea class="form-control" id="msg" rows="6" required></textarea>
                            <div class="invalid-feedback">
                                Por favor, escreva uma mensagem!
                            </div>
                        </div>
                        <button type="submit" class="btn btn-blue-dark form-btn">Enviar</button>
                    </form>
                </div>
                <div class="col-md-5 offset-md-1">

                    <a href="tel:08000178000" title="Ligar para a Osan" class="mt-3 mb-5 d-flex info-container info-tel">
                        <div class="pl-3 pl-lg-5 info-icon-container">
                            <i class="fas fa-phone-alt info-icon info-icon-phone"></i>
                        </div>
                        <div class="pl-3 pr-lg-2 pl-lg-4 info-text">
                            <p><span class="info-text-small">Em caso de falecimento ligue</span><br>
                                0800-017 8000</p>
                        </div>
                    </a>

                    <h2 class="page-wrapper-title">Outras formas de contato</h2>
                    <p class="page-wrapper-text">Para maiores informações, seguem abaixo nossos endereços e telefones para contato a devida adesão.</p>

                    <div class="my-3 my-lg-5">
                        <h3 class="page-wrapper-subtitle">Central Atendimento ao Cliente : CAC</h3>
                        <p class="page-wrapper-text">Dúvidas, atendimentos, solicitações, cartão do associado. <br>
                        Tel.: (13) 3228.8000 / atendimento@osan.com.br</p>
                    </div>

                    <div class="my-3 my-lg-5">
                        <h3 class="page-wrapper-subtitle">Departamento de Cobrança</h3>
                        <p class="page-wrapper-text">Negociação de dívidas acima de 2 débitos. <br>
                        Tel.: (13) 3228.8013</p>
                    </div>

                    <div class="my-3 my-lg-5">
                        <h3 class="page-wrapper-subtitle">Departamento de Faturamento</h3>
                        <p class="page-wrapper-text">2ª via de boletos. <br>
                        Tel.: (13) 3228.8000 / faturamento@osan.com.br</p>
                    </div>

                    <div class="my-3 my-lg-5">
                        <h3 class="page-wrapper-subtitle">Plano Empresarial</h3>
                        <p class="page-wrapper-text">Informações sobre inclusões e exclusões de associados, avisos de falecimentos, faturas para pagamentos e relatórios administrativos.  <br>
                        Tel.: (13) 3228.8019 / planoempresa@osan.com.br</p>
                    </div>

                    <div class="my-3 my-lg-5">
                        <h3 class="page-wrapper-subtitle">Departamento de Vendas</h3>
                        <p class="page-wrapper-text">Tel.: (13) 3228.8001 / vendas@osan.com.br</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
