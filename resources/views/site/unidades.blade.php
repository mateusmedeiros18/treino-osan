@extends('layouts.app')

@section('title', 'Unidades')

@section('content')
    <div class="container page" id="page-unidades">
        <div class="page-nav page-nav-wrapper mb-2 mb-lg-3">
            <div class="container">
                <div class="row">
                    <div class="col-12 pt-2 pt-lg-3">
                        <p class="page-nav-text">Você está em: <a href="{{url('/')}}" class="page-nav-link">Home</a> <a href="{{url('/unidades')}}" class="page-nav-link page-nav-link-active">Unidades</a></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 pt-2 pt-lg-4 pb-2 pb-lg-5">
                        <h1 class="page-wrapper-title">Nossas unidades</h1>
                    </div>
                </div>
            </div>
        </div>

        <div class="container page-wrapper py-4 px-5">
            <div class="row mb-lg-5 unidade-container">
                <!-- Unidade Santos -->
                <div class="col-md-6 col-lg-3 unidade-item">
                    <img src="{{asset('images/osan-santos.png')}}" alt="Unidade da Osan em Santos" class="img-fluid">
                    <div class="unidade-item-title-container">
                        <h2 class="unidade-item-title"><i class="fas fa-map-marker-alt unidade-item-icon"></i> Santos</h2>
                        <a href="https://goo.gl/maps/itEh3gzy9rQxRNU6A" target="_blank" title="Visualizar a localização da unidade Santos no mapa" class="btn btn-outline-blue-dark unidade-btn">Ver no mapa</a>
                    </div>
                    <div class="unidade-item-content">
                        <p class="page-wrapper-text">Rua Quinze de Novembro, 165 <br> Centro - Santos.</p>
                        <div class="unidade-item-phone">
                            <a href="tel:1332288000" title="Ligar para unidade Santos"><i class="fas fa-phone-alt icon-phone icon-phone-blue"></i> <span class="unidade-item-phone-special">(13)</span> 3228.8000</a>
                        </div>
                    </div>
                </div>

                <!-- Unidade São Vicente -->
                <div class="col-md-6 col-lg-3 unidade-item">
                    <img src="{{asset('images/osan-sao_vicente.png')}}" alt="Unidade da Osan em São Vicente" class="img-fluid">
                    <div class="unidade-item-title-container">
                        <h2 class="unidade-item-title"><i class="fas fa-map-marker-alt unidade-item-icon"></i> São Vicente</h2>
                        <a href="https://goo.gl/maps/cR1uqG5S8Tmf2Mdi8" target="_blank" title="Visualizar a localização da unidade São Vicente no mapa" class="btn btn-outline-blue-dark unidade-btn">Ver no mapa</a>
                    </div>
                    <div class="unidade-item-content">
                        <p class="page-wrapper-text">Rua Padre Anchieta, 224 - Centro - São Vicente <br> Rua Lima Machado, 501 - Parque Bitaru. (Rede Conveniada)</p>
                        <div class="unidade-item-phone">
                            <a href="tel:1335692510" title="Ligar para unidade São Vicente"><i class="fas fa-phone-alt icon-phone icon-phone-blue"></i> <span class="unidade-item-phone-special">(13)</span> 3569.2510</a>
                        </div>
                    </div>
                </div>

                <!-- Unidade Cubatão -->
                <div class="col-md-6 col-lg-3 unidade-item">
                    <img src="{{asset('images/osan-cubatao.png')}}" alt="Unidade da Osan em Cubatão" class="img-fluid">
                    <div class="unidade-item-title-container">
                        <h2 class="unidade-item-title"><i class="fas fa-map-marker-alt unidade-item-icon"></i> Cubatão</h2>
                        <a href="https://goo.gl/maps/P6mCfypoWdw1Wtzs9" target="_blank" title="Visualizar a localização da unidade Cubatão no mapa" class="btn btn-outline-blue-dark unidade-btn">Ver no mapa</a>
                    </div>
                    <div class="unidade-item-content">
                        <p class="page-wrapper-text">Av. Martins Fontes, 125 - Vila Nova - Cubatão (Rede Conveniada)</p>
                        <div class="unidade-item-phone">
                            <a href="tel:133569251" title="Ligar para unidade Cubatão"><i class="fas fa-phone-alt icon-phone icon-phone-blue"></i> <span class="unidade-item-phone-special">(13)</span> 3569.251</a>
                        </div>
                    </div>
                </div>

                <!-- Unidade Praia Grande -->
                <div class="col-md-6 col-lg-3 unidade-item">
                    <img src="{{asset('images/osan-praia_grande.png')}}" alt="Unidade da Osan em Praia Grande" class="img-fluid">
                    <div class="unidade-item-title-container">
                        <h2 class="unidade-item-title"><i class="fas fa-map-marker-alt unidade-item-icon"></i> Praia Grande</h2>
                        <a href="https://goo.gl/maps/72dMpjQRgXE2FjpZ9" target="_blank" title="Visualizar a localização da unidade Praia Grande no mapa" class="btn btn-outline-blue-dark unidade-btn">Ver no mapa</a>
                    </div>
                    <div class="unidade-item-content">
                        <p class="page-wrapper-text">Rua Aimorés, 238 - Vl. Tupi - Praia Grande <br> Rua Maria do Carmo Ferro Gomes Ornellas, 82 - Vila Antártica - Praia Grande (Rede Conveniada)</p>
                        <div class="unidade-item-phone">
                            <a href="tel:1332288000" title="Ligar para unidade Praia Grande"><i class="fas fa-phone-alt icon-phone icon-phone-blue"></i> <span class="unidade-item-phone-special">(13)</span> 3228.8000</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row unidade-container">
                <!-- Unidade Guarujá -->
                <div class="col-md-6 col-lg-3 unidade-item">
                    <img src="{{asset('images/osan-guaruja.png')}}" alt="Unidade da Osan em Guarujá" class="img-fluid">
                    <div class="unidade-item-title-container">
                        <h2 class="unidade-item-title"><i class="fas fa-map-marker-alt unidade-item-icon"></i> Guarujá</h2>
                        <a href="https://goo.gl/maps/ocfia8Py6wZZYNvp9" target="_blank" title="Visualizar a localização da unidade Guarujá no mapa" class="btn btn-outline-blue-dark unidade-btn">Ver no mapa</a>
                    </div>
                    <div class="unidade-item-content">
                        <p class="page-wrapper-text">Av. Marco Antônio Oggiano, 24 <br> Morrinhos - Guarujá.</p>
                        <div class="unidade-item-phone">
                            <a href="tel:1335692517" title="Ligar para unidade Guarujá"><i class="fas fa-phone-alt icon-phone icon-phone-blue"></i> <span class="unidade-item-phone-special">(13)</span> 3569.2517</a>
                        </div>
                    </div>
                </div>

                <!-- Unidade Itanhaém -->
                <div class="col-md-6 col-lg-3 unidade-item">
                    <img src="{{asset('images/osan-itanhaem.png')}}" alt="Unidade da Osan em Itanhaém" class="img-fluid">
                    <div class="unidade-item-title-container">
                        <h2 class="unidade-item-title"><i class="fas fa-map-marker-alt unidade-item-icon"></i> Itanhaém</h2>
                        <a href="https://goo.gl/maps/Ksq8weSgSpM3cQnV6" target="_blank" title="Visualizar a localização da unidade Itanhaém no mapa" class="btn btn-outline-blue-dark unidade-btn">Ver no mapa</a>
                    </div>
                    <div class="unidade-item-content">
                        <p class="page-wrapper-text">Av. Rui Barbosa, 810 <br>
                            Centro - Itanhaém</p>
                        <div class="unidade-item-phone">
                            <a href="tel:1335692515" title="Ligar para unidade Itanhaém"><i class="fas fa-phone-alt icon-phone icon-phone-blue"></i> <span class="unidade-item-phone-special">(13)</span> 3569.2515</a>
                        </div>
                    </div>
                </div>

                <!-- Unidade Mongaguá -->
                <div class="col-md-6 col-lg-3 unidade-item">
                    <img src="{{asset('images/osan-mongagua.png')}}" alt="Unidade da Osan em Mongaguá" class="img-fluid">
                    <div class="unidade-item-title-container">
                        <h2 class="unidade-item-title"><i class="fas fa-map-marker-alt unidade-item-icon"></i> Mongaguá</h2>
                        <a href="https://goo.gl/maps/LokwMTmcrBMaH2Cw8" target="_blank" title="Visualizar a localização da unidade Mongaguá no mapa" class="btn btn-outline-blue-dark unidade-btn">Ver no mapa</a>
                    </div>
                    <div class="unidade-item-content">
                        <p class="page-wrapper-text">Av. Marina, 1106 <br> Centro - Mongaguá </p>
                        <div class="unidade-item-phone">
                            <a href="tel:1335692514" title="Ligar para unidade Mongaguá"><i class="fas fa-phone-alt icon-phone icon-phone-blue"></i> <span class="unidade-item-phone-special">(13)</span> 3569.2514</a>
                        </div>
                    </div>
                </div>

                <!-- Unidade Bertioga -->
                <div class="col-md-6 col-lg-3 unidade-item">
                    <img src="{{asset('images/osan-bertioga.png')}}" alt="Unidade da Osan em Bertioga" class="img-fluid">
                    <div class="unidade-item-title-container">
                        <h2 class="unidade-item-title"><i class="fas fa-map-marker-alt unidade-item-icon"></i> Bertioga</h2>
                        <a href="https://goo.gl/maps/Zs8dDMfQbDjNWYJP6" target="_blank" title="Visualizar a localização da unidade Bertioga no mapa" class="btn btn-outline-blue-dark unidade-btn">Ver no mapa</a>
                    </div>
                    <div class="unidade-item-content">
                        <p class="page-wrapper-text">Rua Leonardo de Bonna, 39 - <br> Itapanhaú - Bertioga</p>
                        <div class="unidade-item-phone">
                            <a href="tel:1332282518" title="Ligar para unidade Bertioga"><i class="fas fa-phone-alt icon-phone icon-phone-blue"></i> <span class="unidade-item-phone-special">(13)</span> 3228.2518</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
