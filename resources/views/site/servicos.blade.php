@extends('layouts.app')

@section('title', 'Serviços')

@section('content')
    <div class="container page" id="page-servicos">
        <div class="page-nav page-nav-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-12 pt-2 pt-lg-3">
                        <p class="page-nav-text">Você está em: <a href="{{url('/')}}" class="page-nav-link">Home</a> <a href="{{url('/servicos')}}" class="page-nav-link page-nav-link-active">Serviços</a></p>
                    </div>
                </div>
                <div class="row pt-2 pt-lg-4 pb-2 pb-lg-5">
                    <div class="col-12">
                        <h1 class="page-wrapper-title">Serviços</h1>
                        <h2 class="page-wrapper-subtitle">Conheçam os serviços que oferecemos</h2>
                    </div>
                </div>
            </div>
        </div>

        <div class="container page-wrapper pt-3 pb-5 px-4">
            <div class="row servicos-page">
                <div class="col-md-6 col-lg-3 servicos-item">
                    <a href="{{url('/servicos-integra')}}" class="servicos-link" id="servico-1" title="Conferir serviço">
                        <img src="{{asset('/images/funeral.png')}}" alt="Serviço Funeral" class="d-none d-lg-block servicos-img">
                        <div class="servicos-info">
                            <h3 class="servicos-title">Serviço Funeral</h3>
                            <p class="servicos-desc">Lorem ipsum dolor sit amet, consect adipiscing elit Lorem ipsum dolor.</p>
                            <button type="button" class="btn btn-outline-blue">+ Saiba mais</button>
                        </div>
                    </a>
                </div>

                <div class="col-md-6 col-lg-3 servicos-item">
                    <a href="{{url('/servicos-integra')}}" class="servicos-link" id="servico-2" title="Conferir serviço">
                        <img src="{{asset('/images/translado-terrestre.png')}}" alt="Translado Terrestre" class="d-none d-lg-block servicos-img">
                        <div class="servicos-info">
                            <h3 class="servicos-title">Translado Terrestre</h3>
                            <p class="servicos-desc">Lorem ipsum dolor sit amet, consect adipiscing elit Lorem ipsum dolor.</p>
                            <button type="button" class="btn btn-outline-blue">+ Saiba mais</button>
                        </div>
                    </a>
                </div>

                <div class="col-md-6 col-lg-3 servicos-item">
                    <a href="{{url('/servicos-integra')}}" class="servicos-link" id="servico-3" title="Conferir serviço">
                        <img src="{{asset('/images/translado-aereo.png')}}" alt="Translado Aéreo" class="d-none d-lg-block servicos-img">
                        <div class="servicos-info">
                            <h3 class="servicos-title">Translado Aéreo</h3>
                            <p class="servicos-desc">Lorem ipsum dolor sit amet, consect adipiscing elit Lorem ipsum dolor.</p>
                            <button type="button" class="btn btn-outline-blue">+ Saiba mais</button>
                        </div>
                    </a>
                </div>

                <div class="col-md-6 col-lg-3 servicos-item">
                    <a href="{{url('/servicos-integra')}}" class="servicos-link" id="servico-4" title="Conferir serviço">
                        <img src="{{asset('/images/embalsamento.png')}}" alt="Embalsamento Formolização" class="d-none d-lg-block servicos-img">
                        <div class="servicos-info">
                            <h3 class="servicos-title">Embalsamento Formolização</h3>
                            <p class="servicos-desc">Lorem ipsum dolor sit amet, consect adipiscing elit Lorem ipsum dolor.</p>
                            <button type="button" class="btn btn-outline-blue">+ Saiba mais</button>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
