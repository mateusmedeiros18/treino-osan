@extends('layouts.app')

@section('title', 'Depoimentos')

@section('content')
    <div class="container page" id="page-depoimentos">
        <div class="page-nav page-nav-wrapper mb-2 mb-lg-3">
            <div class="container">
                <div class="row">
                    <div class="col-12 pt-2 pt-lg-3">
                        <p class="page-nav-text">Você está em: <a href="{{url('/')}}" class="page-nav-link">Home</a> <a href="{{url('/depoimentos')}}" class="page-nav-link page-nav-link-active">Depoimentos</a></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 pt-2 pt-lg-4 pb-2 pb-lg-5">
                        <h1 class="page-wrapper-title">Depoimentos</h1>
                    </div>
                </div>
            </div>
        </div>

        <div class="container page-wrapper depoimento-container">
            <div class="row">
                <div class="col-md-6 depoimento-item">
                    <div class="depoimento-item-text">
                        <p>Muito obrigado por tudo. Você se mostrou exímia profissional, de muito tato e delicadeza, excelente energia. Tudo isso trouxe imenso alívio para a família.</p>
                    </div>
                    <div class="depoimento-item-info">
                        <img src="{{asset('images/icones/depoimento.png')}}" class="depoimento-item-info-icon">
                        <p class="depoimento-item-info-author">- Deivid e família</p>
                    </div>
                </div>
                <div class="col-md-6 depoimento-item">
                    <div class="depoimento-item-text">
                        <p>Muito obrigado por tudo. Você se mostrou exímia profissional, de muito tato e delicadeza, excelente energia. Tudo isso trouxe imenso alívio para a família.</p>
                    </div>
                    <div class="depoimento-item-info">
                        <img src="{{asset('images/icones/depoimento.png')}}" class="depoimento-item-info-icon">
                        <p class="depoimento-item-info-author">- Lucas Evedove</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 depoimento-item">
                    <div class="depoimento-item-text">
                        <p>Muito obrigado por tudo. Você se mostrou exímia profissional, de muito tato e delicadeza, excelente energia. Tudo isso trouxe imenso alívio para a família.</p>
                    </div>
                    <div class="depoimento-item-info">
                        <img src="{{asset('images/icones/depoimento.png')}}" class="depoimento-item-info-icon">
                        <p class="depoimento-item-info-author">- Deivid e família</p>
                    </div>
                </div>
                <div class="col-md-6 depoimento-item">
                    <div class="depoimento-item-text">
                        <p>Muito obrigado por tudo. Você se mostrou exímia profissional, de muito tato e delicadeza, excelente energia. Tudo isso trouxe imenso alívio para a família.</p>
                    </div>
                    <div class="depoimento-item-info">
                        <img src="{{asset('images/icones/depoimento.png')}}" class="depoimento-item-info-icon">
                        <p class="depoimento-item-info-author">- Lucas Evedove</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 depoimento-item">
                    <div class="depoimento-item-text">
                        <p>Muito obrigado por tudo. Você se mostrou exímia profissional, de muito tato e delicadeza, excelente energia. Tudo isso trouxe imenso alívio para a família.</p>
                    </div>
                    <div class="depoimento-item-info">
                        <img src="{{asset('images/icones/depoimento.png')}}" class="depoimento-item-info-icon">
                        <p class="depoimento-item-info-author">- Deivid e família</p>
                    </div>
                </div>
                <div class="col-md-6 depoimento-item">
                    <div class="depoimento-item-text">
                        <p>Muito obrigado por tudo. Você se mostrou exímia profissional, de muito tato e delicadeza, excelente energia. Tudo isso trouxe imenso alívio para a família.</p>
                    </div>
                    <div class="depoimento-item-info">
                        <img src="{{asset('images/icones/depoimento.png')}}" class="depoimento-item-info-icon">
                        <p class="depoimento-item-info-author">- Lucas Evedove</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="container pagination-container">
            <nav aria-label="Navegação das perguntas frequentes">
                <ul class="pagination justify-content-center">
                    <li class="page-item disabled">
                        <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Anterior</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Anterior">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Próximo">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" href="#">Próximo</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
@endsection
