/*--------------
--- REQUIRES ---
--------------*/
const gulp = require('gulp'),
    autoprefixer = require('gulp-autoprefixer'),
    concat = require('gulp-concat'),
    imagemin = require('gulp-imagemin'),
    sass = require('gulp-sass');

sass.compiler = require('node-sass');

/*---------------
----- PATHS -----
---------------*/
const paths = {
    dev: {
        fonts: "resources/assets/fonts/**/*",
        img: "resources/assets/images/**/*",
        js: "resources/assets/js/*.js",
        sass: "resources/assets/sass/*.scss",
    },
    dist: {
        fonts: "public/fonts",
        img: "public/images",
        js: "public/js",
        css: "public/css",
    }
}

/*--------------
---- ASSETS ----
--------------*/
const assets = {
    css: [
        "node_modules/bootstrap/dist/css/bootstrap.min.css",
        "node_modules/@fortawesome/fontawesome-free/css/all.min.css",
        "node_modules/slick-carousel/slick/slick.css",
        "node_modules/slick-carousel/slick/slick-theme.css"
    ],
    js: [
        "node_modules/bootstrap/dist/js/bootstrap.min.js",
        "node_modules/@fortawesome/fontawesome-free/js/all.min.js",
        "node_modules/slick-carousel/slick/slick.min.js"
    ],
}

/*-------------
-- FUNCTIONS --
-------------*/
// Compilar SASS
function compSass() {
    return gulp
        .src(paths.dev.sass)
        .pipe(sass({ outputStyle: 'compressed' }).on("error", sass.logError))
        .pipe(autoprefixer({ cascade: false }))
        .pipe(gulp.dest(paths.dist.css));
}

// Importar fontes
function importFonts() {
    return gulp
        .src(paths.dev.fonts)
        .pipe(gulp.dest(paths.dist.fonts));
}

// Importar CSS dos assets
function importCSSAssets() {
    return gulp
        .src(assets.css)
        .pipe(gulp.dest(`${paths.dist.css}/libs`));
}

// Importar JS dos assets
function importJSAssets() {
    return gulp
        .src(assets.js)
        .pipe(gulp.dest(`${paths.dist.js}/libs`));
}

// Minificar imagens
function minImg() {
    return gulp
        .src(paths.dev.img)
        .pipe(imagemin())
        .pipe(gulp.dest(paths.dist.img))
}

// Concatenar js
function minJs() {
    return gulp
        .src(paths.dev.js)
        .pipe(concat('main.js'))
        .pipe(gulp.dest(paths.dist.js))
}

// Assistir arquivos
function watch() {
    gulp.watch(paths.dev.sass, compSass);
    gulp.watch(paths.dev.fonts, importFonts);
    gulp.watch(paths.dev.img, minImg);
    gulp.watch(paths.dev.js, minJs);
}

/*-------------
---- TASKS ----
-------------*/
gulp.task("default", watch);
gulp.task("assets", gulp.series(importCSSAssets, importJSAssets));
gulp.task("fonts", importFonts);
gulp.task("image", minImg);
gulp.task("js", minJs);
gulp.task("sass", compSass);
